package com.example.alessandro.exception;

/**
 * Created by Alessandro on 26/07/2016.
 */
public class MyExceptionInsert extends Exception{
    private String message;
    public MyExceptionInsert(String message){
        this.message = message;
    }

    @Override
    public String getMessage(){
        return message;
    }

}
