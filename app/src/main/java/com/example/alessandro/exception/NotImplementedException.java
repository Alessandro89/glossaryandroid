package com.example.alessandro.exception;

/**
 * Created by Alessandro on 08/09/2016.
 */
public class NotImplementedException extends Exception {
    private String message;
    public NotImplementedException(String message){
        this.message = message;
    }
    @Override
    public String getMessage(){
        return message;
    }
}
