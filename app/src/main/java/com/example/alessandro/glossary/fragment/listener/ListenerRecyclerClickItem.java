package com.example.alessandro.glossary.fragment.listener;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.example.alessandro.glossary.GlossaryActivity;
import com.example.alessandro.glossary.R;
import com.example.alessandro.glossary.fragment.AdapterRecycler;

/**
 * Created by Alessandro on 23/09/2016.
 */
public class ListenerRecyclerClickItem<T> implements View.OnClickListener{

    private int positionItem;
    private AdapterRecycler<T>  adapterRecycler;
    private Bundle bundle;
    private Fragment fragment;

    public ListenerRecyclerClickItem(int positionItem, AdapterRecycler<T> adapterRecycler, Fragment f, Bundle bundleArgument) {
        this.positionItem = positionItem;
        this.adapterRecycler = adapterRecycler;
        this.fragment = f;
        this.bundle = bundleArgument;
    }

    @Override
    public void onClick(View v) {
        changeFragment();
    }

    private void changeFragment() {
        Activity thisactivity = ((AdapterRecycler<T>) adapterRecycler).getActivity();
        FragmentManager fm = ((FragmentActivity) (thisactivity)).getSupportFragmentManager(); //getFragmentManager();
        
        if(bundle!=null) {
            this.fragment.setArguments(bundle);
        }
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.containerfragment, this.fragment, ((GlossaryActivity) thisactivity).getTagCurrentFragment());
        ft.addToBackStack(null);
        ft.commit();
    }
}
