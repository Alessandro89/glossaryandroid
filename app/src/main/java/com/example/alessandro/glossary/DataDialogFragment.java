package com.example.alessandro.glossary;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.ColorUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Alessandro on 23/07/2016.
 */
public class DataDialogFragment extends DialogFragment {


    private ArrayList<String> listSingleChoice;

    /* The activity that creates an instance of this dialog fragment must
         * implement this interface in order to receive event callbacks.
         * Each method passes the DialogFragment in case the host needs to query it. */
    public interface NoticeDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog, ArrayList<String> listSelectedCategory, String elementChoice, DialogType typeDialog);
        public void onDialogNegativeClick(DialogFragment dialog, DialogType typeDialog);
        public void onDismissClick(DialogType typeDialog);
    }



    // Use this instance of the interface to deliver action events
    private NoticeDialogListener mListener;
    private DialogType typeDialog;
    //private ArrayAdapter arrayAdapterGlossary;
    private ArrayList<String> listCategories;
    private ArrayList<String> alreadyselectedItemsGlossary;
    private String word;
    private int positionListViewMeans;

    public void setDialogType(DialogType typeDialog){
        this.typeDialog = typeDialog;
    }

    public void setListCategories(ArrayList<String> listCategories, ArrayList<String> alreadyselectedItemsGlossary){
        this.listCategories = listCategories;
        this.alreadyselectedItemsGlossary = alreadyselectedItemsGlossary;
    }

    public void setListSingleChoice(ArrayList<String> listSingleChoice){
        this.listSingleChoice = listSingleChoice;
    }

    public void setWord(String word){
        this.word = word;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //DA usare una FACTORY!
        final ArrayList selectedCategoryItems = new ArrayList();  // Where we track the selected items

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout

        builder.setTitle(getArguments().getString("testo"));



        if(this.typeDialog == DialogType.SingleChoiceList){
            builder.setItems(listSingleChoice.toArray(new CharSequence[listSingleChoice.size()]), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // The 'which' argument contains the index position
                    // of the selected item
                    mListener.onDialogPositiveClick(DataDialogFragment.this,selectedCategoryItems, listSingleChoice.get(which), typeDialog);
                }
            });

            //la view qui è quella di default per ora..
        }
        else if(this.typeDialog == DialogType.Category) {
            builder.setMultiChoiceItems(listCategories.toArray(new CharSequence[listCategories.size()]), getCheckedGlossaryValues(),
                    new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which,
                                            boolean isChecked) {
                            if (isChecked) {
                                // If the user checked the item, add it to the selected items
                                selectedCategoryItems.add(listCategories.get(which));
                            } else if (selectedCategoryItems.contains(which)) {
                                // Else, if the item is already in the array, remove it
                                selectedCategoryItems.remove(listCategories.get(which));
                            }
                        }
                    });


            builder.setView(inflater.inflate(R.layout.dialog_insertcategory, null));
        }
        else{
            View customview = inflater.inflate(R.layout.dialog_insertelement, null);
            if(this.typeDialog == DialogType.Word){
                ((EditText)customview.findViewById(R.id.textDialog)).setText(word);
            }
            builder.setView(customview);
        }

        if(typeDialog!=DialogType.SingleChoiceList) {

            builder.setPositiveButton("Aggiungi", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    mListener.onDialogPositiveClick(DataDialogFragment.this, selectedCategoryItems, null, typeDialog);
                    // User clicked OK button
                }
            });
        }

        builder.setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                mListener.onDialogNegativeClick(DataDialogFragment.this, typeDialog);

            }
        });

       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
      /      builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    mListener.onDismissClick(typeDialog);
                }
            });
        }*/

        // 3. Get the AlertDialog from create()

        AlertDialog aldialog = builder.create();
        if(aldialog.getListView()!=null){
            ListView lv = aldialog.getListView();
            lv.setScrollbarFadingEnabled(false);
            ColorDrawable color = new ColorDrawable();
            color.setColor(Color.rgb(0,120,0));
            lv.setDivider(color);
            lv.setDividerHeight(1);

            //Object item1 = lv.getItemAtPosition(0);
            //View view1 = lv.getChildAt(0);
            //((TextView) lv.getChildAt(0)).setTextColor(Color.GRAY);
        }
        /*if(this.typeDialog == DialogType.Category){
            ListView lv = aldialog.getListView();
            lv.setScrollbarFadingEnabled(false);
            ColorDrawable color = new ColorDrawable();
            color.setColor(Color.DKGRAY);
            lv.setDivider(color);
            lv.setDividerHeight(1);
            //LayoutParams layoutParams = lv.getLayoutParams();
            //layoutParams.height = 150;
            //lv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 30)); (??)
        }*/


        return aldialog;
    }

    private boolean[] getCheckedGlossaryValues(){
        boolean[] arrayElementsboolen = new boolean[listCategories.size()];
        int i =0;
        for(String elCategory: this.listCategories){
            if(this.alreadyselectedItemsGlossary.contains(elCategory)){
                arrayElementsboolen[i] = true;
            }else{
                arrayElementsboolen[i] = false;
            }
            i++;
        }
        return arrayElementsboolen;
    }

    @Override //for api <23
    public void onAttach(Activity activity) {
        Log.d("tag", "onattach called");
        super.onAttach(activity);
        attachlistener((FragmentActivity) activity);
    }
    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Context context) {
        Log.d("tag", "onattach called");
        super.onAttach(context);
        Activity activity = (Activity) context;
        // Verify that the host activity implements the callback interface
        attachlistener((FragmentActivity) activity);
    }

    private void attachlistener(FragmentActivity activity) {
        Fragment fr = null;
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host

            //cambio da activity a fragments, il fragment dato che implementa tale interfaccia è anche di tipo NoticeDialogListener
            FragmentManager fm = activity.getSupportFragmentManager(); //getFragmentManager();
            //l'unica activity in cui viene passata è il glossary activity, per cui nessun problema..
            fr = (Fragment) fm.findFragmentByTag( ((GlossaryActivity) activity).getTagCurrentFragment());

            FragmentManager fmchild = fr.getChildFragmentManager();

            //se è un subfragment:
            if(fmchild!=null && fmchild.getFragments() !=null && fmchild.getFragments().size()>0) {
                fr = (Fragment) fmchild.findFragmentByTag( ((GlossaryActivity) activity).getTagCurrentSubFragment());
                mListener = (NoticeDialogListener) fr;
            }
            else {
                mListener = (NoticeDialogListener) fr;
            }

        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException("your fragment "+ fr.toString()+"  must implement NoticeDialogListener");
        }catch (Exception e){
            //nel caso per esempio che non trova il fragment (quando giri lo schermo con il dialog aperto)
            this.dismiss();
        }
    }


    @Override
    public void onDismiss(DialogInterface dialogInterface){
        if(mListener!=null)
            mListener.onDismissClick(this.typeDialog);
        super.onDismiss(dialogInterface);
    }


}
