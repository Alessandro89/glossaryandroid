package com.example.alessandro.glossary.fragment.showwords;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alessandro.glossary.GlossaryActivity;
import com.example.alessandro.glossary.ModelDbWord;
import com.example.alessandro.glossary.R;
import com.example.alessandro.glossary.model.Translation;
import com.example.alessandro.glossary.model.Word;
import com.google.gson.Gson;

import java.util.ArrayList;


public class FragmentShowDailyWord extends Fragment {


    private Gson gson = new Gson();

 @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       return inflater.inflate(R.layout.fragment_showdailywords, container, false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FragmentActivity thisactivity = getActivity();
        ModelDbWord modelDbWord = new ModelDbWord(thisactivity.getApplicationContext());

        Translation tr = ((GlossaryActivity) getActivity()).getCurrentTranslation();
        ArrayList<Word> listWords = modelDbWord.lightGetAllDailyWords(tr.getIdTranslation());
        RecyclerView dailyWordRecyclerView = (RecyclerView) thisactivity.findViewById(R.id.recyclerViewDailyWord);
        AdapterRecyclerDailyWords dailyWordAdapterRecycler = new AdapterRecyclerDailyWords(listWords, thisactivity);
        dailyWordRecyclerView.setLayoutManager(new LinearLayoutManager(thisactivity));
        dailyWordRecyclerView.setAdapter(dailyWordAdapterRecycler);

        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(this.getContext(),
                LinearLayoutManager.VERTICAL);
        dailyWordRecyclerView.addItemDecoration(mDividerItemDecoration);


    }
}
