package com.example.alessandro.glossary.fragment.showwords;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alessandro.glossary.ModelDbWord;
import com.example.alessandro.glossary.R;

import java.util.ArrayList;
import java.util.List;


public class SubFragmentShowExample extends Fragment{
    private RecyclerView recyclerViewExample;
    private AdapterRecyclerShowCategory exampleAdapterRecycler;
    private int idMean;
    private Activity myActivity;

    public SubFragmentShowExample() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_showdatamean_example, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(getArguments()!=null){ //gli arguments rimangono anche dopo il destroyview.
            this.idMean = getArguments().getInt("idMean");
        }

        ModelDbWord mdw = new ModelDbWord(myActivity.getApplicationContext());

        List<String> exampleList = mdw.getAllExampleByIdMean(idMean);
        exampleAdapterRecycler = new AdapterRecyclerShowCategory((ArrayList<String>) exampleList, myActivity);

        recyclerViewExample = (RecyclerView) myActivity.findViewById(R.id.recyclerViewExample);
        recyclerViewExample.setLayoutManager(new LinearLayoutManager(myActivity));
        recyclerViewExample.setVerticalScrollBarEnabled(true);
        recyclerViewExample.setAdapter(exampleAdapterRecycler);

        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(this.getContext(),
                LinearLayoutManager.VERTICAL);
        recyclerViewExample.addItemDecoration(mDividerItemDecoration);
    }


    @Override
    public void onAttach(Context context){
        super.onAttach(context);

        try {
            //mi aspetto che il context sia come istanza una activity
            this.myActivity = (Activity) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }
}