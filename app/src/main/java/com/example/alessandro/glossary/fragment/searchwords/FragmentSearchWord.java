package com.example.alessandro.glossary.fragment.searchwords;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.alessandro.glossary.ModelDbWord;
import com.example.alessandro.glossary.R;
import com.example.alessandro.glossary.fragment.insertword.TabsPageAdapterMeanInsert;


public class FragmentSearchWord extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    //private ListView glossaryListView;


    public FragmentSearchWord() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_words, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FragmentActivity thisactivity = getActivity();

        viewPager = (ViewPager) thisactivity.findViewById(R.id.pager);
        PagerAdapter pageradapter = new TabsPageAdapterSearchWord(this.getChildFragmentManager(),thisactivity);
        viewPager.setAdapter(pageradapter);

        tabLayout = (TabLayout) thisactivity.findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);

    }
}