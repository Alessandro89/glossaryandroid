package com.example.alessandro.glossary.fragment.searchwords;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alessandro.glossary.GlossaryActivity;
import com.example.alessandro.glossary.ModelDbWord;
import com.example.alessandro.glossary.R;
import com.example.alessandro.glossary.fragment.insertword.listener.PlayerListener;
import com.example.alessandro.glossary.fragment.showwords.AdapterRecyclerShowMean;
import com.example.alessandro.glossary.model.Mean;
import com.example.alessandro.glossary.model.Translation;
import com.example.alessandro.glossary.model.Word;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;


public class SubFragmentSearchWordByName extends Fragment implements View.OnClickListener {

    private Activity myActivity;
    private EditText editTextWordSearched;
    private String wordSearched;
    private AdapterRecyclerShowMean adapterMean;

    private ModelDbWord mdw;
    private String wordFound;
    private boolean thereIsSound = false;
    private String soundPath = null;
    private RecyclerView recyclerViewMeans;

    public SubFragmentSearchWordByName() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        if(savedInstanceState!=null) {
            wordSearched = savedInstanceState.getString("word");
            String gsonListMean = savedInstanceState.getString("gsonListMeanByWord");

            wordFound = savedInstanceState.getString("wordFound");
            thereIsSound = savedInstanceState.getBoolean("thereIsSound");
            soundPath = savedInstanceState.getString("soundPath");

            if(gsonListMean!=null) {
                Type typeListMean = new TypeToken<ArrayList<Mean>>() {
                }.getType();

                ArrayList<Mean> listMean = new Gson().fromJson(gsonListMean, typeListMean);
                adapterMean = new AdapterRecyclerShowMean(listMean, myActivity);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_words_byword, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FloatingActionButton btnSearch = (FloatingActionButton) myActivity.findViewById(R.id.btnSearchWordForName);
        editTextWordSearched = (EditText) myActivity.findViewById(R.id.editSearchTextParola);
        if(wordSearched!=null) //e' stato ripristinato (oncreate), oppure siamo tornati indietro dal backstack
            editTextWordSearched.setText(wordSearched);

        if(adapterMean!=null && adapterMean.getItems().size()>0) {
            //se ci sono dei significati
            recyclerViewMeans = (RecyclerView) myActivity.findViewById(R.id.recyclerViewShowMeans);
            recyclerViewMeans.setLayoutManager(new LinearLayoutManager(myActivity));
            recyclerViewMeans.setAdapter(adapterMean);

            DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(this.getContext(),
                    LinearLayoutManager.VERTICAL);
            recyclerViewMeans.addItemDecoration(mDividerItemDecoration);
        }
        if(wordFound!=null){
            //se è stata trovata la parola
            ImageButton btnPlayOff = (ImageButton) myActivity.findViewById(R.id.btnSoundOff);
            ImageButton btnPlay = (ImageButton) myActivity.findViewById(R.id.btnSound);
            TextView textViewWordFound = (TextView) myActivity.findViewById(R.id.textViewWordFound);
            textViewWordFound.setText(wordFound);

            if(thereIsSound){
                btnPlay.setVisibility(View.VISIBLE);
                btnPlayOff.setVisibility(View.INVISIBLE);
                btnPlay.setOnClickListener(new PlayerListener(soundPath));
            }else{
                btnPlay.setVisibility(View.INVISIBLE);
                btnPlayOff.setVisibility(View.VISIBLE);
            }

        }

        mdw = new ModelDbWord(myActivity.getApplicationContext());

        btnSearch.setOnClickListener(this);

    }


    @Override
    public void onAttach(Context context){
        super.onAttach(context);

        try {
            //mi aspetto che il context sia come istanza una activity
            this.myActivity = (Activity) context;

        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btnSearchWordForName){
            TextView textViewWordFound = (TextView) myActivity.findViewById(R.id.textViewWordFound);
            ImageButton btnPlayOff = (ImageButton) myActivity.findViewById(R.id.btnSoundOff);
            ImageButton btnPlay = (ImageButton) myActivity.findViewById(R.id.btnSound);

            String wordname = editTextWordSearched.getText().toString();
            //Translation tr = ((GlossaryActivity) getActivity()).getCurrentTranslation();
            //Word wd = mdw.lightGetWordByName(tr.getIdTranslation(),wordname);
            Word wd = mdw.lightGetWordByName(wordname);
            if(wd!=null){
                int idWord = wd.getId();
                textViewWordFound.setText(wd.getName());
                wordFound = wd.getName();

                ArrayList<Mean> listMean = mdw.lightGetAllMeansByIdWord(idWord);
                adapterMean = new AdapterRecyclerShowMean(listMean,myActivity);
                recyclerViewMeans = (RecyclerView) myActivity.findViewById(R.id.recyclerViewShowMeans);
                recyclerViewMeans.setLayoutManager(new LinearLayoutManager(myActivity));
                recyclerViewMeans.setAdapter(adapterMean);
                DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(this.getContext(),
                        LinearLayoutManager.VERTICAL);
                recyclerViewMeans.addItemDecoration(mDividerItemDecoration);



                //mettiamo il suono:
                if(wd.getSoundPathWord()!=null){ //se giro lo schermo?
                    btnPlay.setVisibility(View.VISIBLE);
                    btnPlayOff.setVisibility(View.INVISIBLE);
                    thereIsSound = true;
                    btnPlay.setOnClickListener(new PlayerListener(wd.getSoundPathWord()));
                    soundPath = wd.getSoundPathWord();
                }else{
                    thereIsSound = false;
                    btnPlay.setVisibility(View.INVISIBLE);
                    btnPlayOff.setVisibility(View.VISIBLE);
                }

            }else{
                showToast("nessuna parola trovata");
                //nessuna parola trovata
                wordFound = null;
                thereIsSound = false;
                soundPath = null;

                textViewWordFound.setText("");
                btnPlay.setVisibility(View.INVISIBLE);
                btnPlayOff.setVisibility(View.INVISIBLE);

                if(recyclerViewMeans!=null) {
                    adapterMean = new AdapterRecyclerShowMean(new ArrayList<Mean>(),myActivity);
                    recyclerViewMeans.setLayoutManager(new LinearLayoutManager(myActivity));
                    recyclerViewMeans.setAdapter(adapterMean);
                }
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState){

        Gson gson = new Gson();
        if (adapterMean != null) { //se sono stati trovati dei significati
            String gsonListMeanByWord = gson.toJson(adapterMean.getItems());
            outState.putString("gsonListMeanByWord", gsonListMeanByWord);
        }
        if(wordFound!=null){ //se è stata trovata una parola:
            //ci affidiamo alle variabili, se siamo nel back stack le viste possono essere nulle:..
            outState.putString("wordFound", wordFound);
            outState.putBoolean("thereIsSound", thereIsSound);
            outState.putString("soundPath", soundPath);
        }

        if(editTextWordSearched !=null)
            outState.putString("searchedWord",editTextWordSearched.getText().toString());
        else
            outState.putString("searchedWord", wordSearched);

        super.onSaveInstanceState(outState);
    }

    private void showToast(String message) {
        Toast toast = Toast.makeText(myActivity, message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}