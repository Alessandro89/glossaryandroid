package com.example.alessandro.glossary.fragment.insertword;

import android.animation.StateListAnimator;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.alessandro.exception.MyExceptionInsert;
import com.example.alessandro.glossary.DataDialogFragment;
import com.example.alessandro.glossary.DialogType;
import com.example.alessandro.glossary.DictionaryAdvice;
import com.example.alessandro.glossary.GlossaryActivity;
import com.example.alessandro.glossary.ModelDbWord;
import com.example.alessandro.glossary.R;
import com.example.alessandro.glossary.fragment.insertword.adapter.AdapterRecyclerInsertMean;
import com.example.alessandro.glossary.fragment.insertword.listener.ListMeanListener;
import com.example.alessandro.glossary.fragment.insertword.listener.PlayerListener;
import com.example.alessandro.glossary.fragment.insertword.listener.RecorderListener;
import com.example.alessandro.glossary.model.Mean;
import com.example.alessandro.glossary.model.Translation;
import com.example.alessandro.glossary.model.Word;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class FragmentInsertWord extends Fragment implements DataDialogFragment.NoticeDialogListener, View.OnClickListener {

   // private OnFragmentInteractionListener mListener;

    // Container Activity must implement this interface
    private ListMeanListener listMeanListener;
    private ModelDbWord modelDbWord;
    private EditText wordTextEd;
    private String word;
    private String timeChr;
    private String absoluteTempPathAudioWord;
    private boolean alreadySearchedOnNetwork = false;


    private RecyclerView meansRecyclerView;
    private AdapterRecyclerInsertMean meanAdapterRecycler;
    private Gson gson = new Gson();
    private Chronometer chr;
    private ImageButton btnPlayer,btnResetAudio;
    private boolean isExistFileTempSound = false;
    private Toast currentToast;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        absoluteTempPathAudioWord = getContext().getFilesDir().getAbsolutePath();
        absoluteTempPathAudioWord+= "_tempThisWord.mpeg4";
        File fileTempSound = new File(absoluteTempPathAudioWord);

        if(savedInstanceState!=null){ //recupero i dati che erano presenti prima della sua distruzione
            //prendiamo la lista dall'activity
            restoreData(savedInstanceState);

            //verifico se avevamo registrato qualcosa..
            if(fileTempSound.exists()){
                isExistFileTempSound = true;
            }

        }
        else{ //sei in un primo avvio, savedInstanceState è null
            if(fileTempSound.exists()){
                fileTempSound.delete(); //cancelliamo tale file!, è una registrazione vecchia
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View thisView = inflater.inflate(R.layout.fragment_insert_words, container, false);


        return thisView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        createListener(getActivity());

        FragmentActivity thisactivity = getActivity();
        modelDbWord = new ModelDbWord(thisactivity.getApplicationContext());
        //modelDbWord.deletedatabaseForTest(); //cancello tutto

        Button btnInsert = (Button) thisactivity.findViewById(R.id.btInsertWord);
        ImageButton btnAddMean = (ImageButton) thisactivity.findViewById(R.id.btnAddMean);
        ImageButton btnEditWord = (ImageButton) thisactivity.findViewById(R.id.btnEditWord);
        ImageButton btnSuggestMean = (ImageButton) thisactivity.findViewById(R.id.fabSuggestMean);
        ImageButton btnRecorder = (ImageButton) thisactivity.findViewById(R.id.fabRecorder);
        btnPlayer = (ImageButton) thisactivity.findViewById(R.id.fabPlayAudio);
        btnResetAudio = (ImageButton) thisactivity.findViewById(R.id.fabDeleteAudio);
        chr = (Chronometer) thisactivity.findViewById(R.id.chrRecording);

        //di default le 2 viste sono invisibile, e isExistFileTempSound è a false
        if(isExistFileTempSound) {
            btnPlayer.setVisibility(View.VISIBLE);
            btnResetAudio.setVisibility(View.VISIBLE);
            chr.setVisibility(View.VISIBLE);
        }

        wordTextEd = (EditText) thisactivity.findViewById(R.id.editInsertWord);

        //prendiamo la lista dall'activity
        ArrayList<Mean> listMean = this.listMeanListener.getListMeans();
        meansRecyclerView = (RecyclerView) thisactivity.findViewById(R.id.recyclerViewMean);
        meanAdapterRecycler = new AdapterRecyclerInsertMean(listMean, wordTextEd, thisactivity);
        meansRecyclerView.setLayoutManager(new LinearLayoutManager(thisactivity));
        meansRecyclerView.setAdapter(meanAdapterRecycler);

        //aggiungiamo il separatore
       DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(this.getContext(),
               LinearLayoutManager.VERTICAL);
        meansRecyclerView.addItemDecoration(mDividerItemDecoration);

        btnInsert.setOnClickListener(this);
        btnAddMean.setOnClickListener(this);
        btnSuggestMean.setOnClickListener(this);
        btnEditWord.setOnClickListener(this);


        RecorderListener recorderListener = new RecorderListener(absoluteTempPathAudioWord,
                chr, btnPlayer,btnResetAudio, btnInsert);
        PlayerListener playerListener = new PlayerListener(absoluteTempPathAudioWord);
        DeleteListener deleteListener = new DeleteListener(absoluteTempPathAudioWord, chr, btnPlayer,btnResetAudio);
        btnRecorder.setOnClickListener(recorderListener);
        btnPlayer.setOnClickListener(playerListener);
        btnResetAudio.setOnClickListener(deleteListener);


        if(savedInstanceState!=null) {
            wordTextEd.setText(word);
            chr.setText(timeChr);
        }

    }

    private void restoreData(Bundle savedInstanceState){
        word = savedInstanceState.getString("word");
        timeChr = savedInstanceState.getString("timeChr");

    }

    @Override
    public void onClick(View v){

        if(v.getId() == R.id.btInsertWord) {
            try{
                insertWord();
                showToast("la parola e' stata inserita");
                cleanDataAndInterface();
            }catch (MyExceptionInsert ex){
                Log.d("error: ",ex.getMessage());
                ex.printStackTrace();
                showToast(ex.getMessage());
            }
        }
        else if(v.getId() == R.id.btnEditWord){
            showTheDialog("Inserisci la parola da tradurre: ", DialogType.Word,null);
        }
        else if(v.getId() == R.id.btnAddMean){
            showTheDialog("Aggiungi un significato: ", DialogType.Mean,null);
        }else if(v.getId() == R.id.fabSuggestMean){
            if(isConnected()) {
                if(!alreadySearchedOnNetwork) {//solo un click per volta
                    String wordInserting = wordTextEd.getText().toString();
                    if (!wordInserting.equals("")) {
                        String[] translationFromTo = ((GlossaryActivity) getActivity()).getCurrentTranslation().getDescription().split("-");
                        new ReadMeaningOfWord().execute(translationFromTo[0], translationFromTo[1], wordInserting);
                    }
                    else
                        showToast("scrivi una parola..");
                }
            }
            else{
                showToast("you are not connected!");
            }
        }
    }


    private void showToast(String message) {
        if(currentToast!=null)
            currentToast.cancel();
        currentToast = Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT);
        currentToast.setGravity(Gravity.CENTER, 0, 0);
        currentToast.show();
    }

    private void showTheDialog(String text, final DialogType dialogType, ArrayList<String> listChoice){

        DialogFragment dialog = new DataDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putString("testo", text);
        dialog.setArguments(bundle);

        if(dialogType == DialogType.Word){
            ((DataDialogFragment)dialog).setWord(wordTextEd.getText().toString());
        }
        if(dialogType == DialogType.SingleChoiceList){
            ((DataDialogFragment)dialog).setListSingleChoice(listChoice);
        }
        ((DataDialogFragment) dialog).setDialogType(dialogType);
        dialog.show(getActivity().getFragmentManager(), "DataDialogFragment");

    }

    private ArrayList<String> getElementsOfArrayAdaper(ArrayAdapter<String> arrayAdapterParam){
        ArrayList<String> list = new ArrayList<String>();
        for(int i = 0; i<arrayAdapterParam.getCount();i++){
            list.add(arrayAdapterParam.getItem(i));
        }
        return list;
    }

    private void insertWord() throws MyExceptionInsert {
        String word = wordTextEd.getText().toString();

        if(!word.equals("")) {
            File soundTmp = new File(absoluteTempPathAudioWord);
            String soundWordAbsolutePath = null;

            if(soundTmp.exists()) {
                soundWordAbsolutePath = getContext().getFilesDir() + word + ".mpeg";

                File soundWord = new File(soundWordAbsolutePath);
                new SaveFileSound().execute(soundTmp, soundWord);
            }
            //id translation da implementare
            Translation tr = ((GlossaryActivity) getActivity()).getCurrentTranslation();
            Word wd = new Word(tr,word, soundWordAbsolutePath, new Date().getTime(), listMeanListener.getListMeans());

            try {
                modelDbWord.insertWord(wd);
            }catch (MyExceptionInsert e){
                //se c'è stato un errore di inserimento cancelliamo il file sound..
                if(soundWordAbsolutePath!=null)
                    new File(soundWordAbsolutePath).delete();
                //propaghiamo l'errore..
                throw e;
            }
        }
        else{
            throw new MyExceptionInsert("inserire una parola da tradurre");
        }
    }


    private void cleanDataAndInterface(){
        wordTextEd.setText("");
        //pulisco l'adapter
        this.meanAdapterRecycler.getItems().clear(); //rimuovo tutti gli item, la lisa è vuota adesso
        this.meanAdapterRecycler.notifyDataSetChanged();
        //setto a lista vuota anche la listMean dell'activity:
        this.listMeanListener.setListMeans(this.meanAdapterRecycler.getItems());

        chr.setVisibility(View.INVISIBLE);
        btnPlayer.setVisibility(View.INVISIBLE);
        btnResetAudio.setVisibility(View.INVISIBLE);
    }


    @Override
    public void onDialogPositiveClick(DialogFragment dialog, ArrayList<String> listSelectedCategory, String singleChoice, DialogType dialogType) {
        String data = null;
        if(dialogType!=DialogType.SingleChoiceList)
            data = ((EditText)dialog.getDialog().findViewById(R.id.textDialog)).getText().toString();
        //else
       //     alreadySearchedOnNetwork = false;

        switch (dialogType){
            case Mean:
                addMeanAndFocusOnLastElement(data);
                break;
            case Word:
                wordTextEd.setText(data);
                break;
            case SingleChoiceList:
                addMeanAndFocusOnLastElement(singleChoice);
                break;
        }

        hideKeyboardFromEditText((EditText) dialog.getDialog().findViewById(R.id.textDialog));
    }

    private void addMeanAndFocusOnLastElement(String meanStr) {
        if(!meanStr.equals("")) {
            Mean m = new Mean();
            m.setMean(meanStr);
            meanAdapterRecycler.addItem(m);
            meanAdapterRecycler.notifyDataSetChanged();
            //setto la lista
            listMeanListener.setListMeans(meanAdapterRecycler.getItems());
            focusOnLastElementOnRecyclerView(this.meansRecyclerView);
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog, DialogType dialogType) {
        hideKeyboardFromEditText((EditText) dialog.getDialog().findViewById(R.id.textDialog));
    }

    @Override
    public void onDismissClick(DialogType typeDialog) {
        if(typeDialog.equals(DialogType.SingleChoiceList))
            alreadySearchedOnNetwork = false;
    }

    private void focusOnLastElementOnRecyclerView(RecyclerView lrecyclerView) {
        lrecyclerView.smoothScrollToPosition(lrecyclerView.getAdapter().getItemCount() - 1);
    }

    private void hideKeyboardFromEditText(EditText windowEdit){
        if(windowEdit!=null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE); //prendiamo l'input method manager
            imm.hideSoftInputFromWindow(windowEdit.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState){
        if(wordTextEd!=null) { //non siamo in questo fragment ma in uno in backstack
            outState.putString("word", wordTextEd.getText().toString());
            outState.putString("timeChr", chr.getText().toString());
        }
        else { //siamo in un altro fragment, le viste sono null..
            outState.putString("timeChr", timeChr);
            outState.putString("word", word);
        }

        super.onSaveInstanceState(outState);
    }

    public RecyclerView getMeansRecyclerView() {
        return meansRecyclerView;
    }

    public void setMeansRecyclerView(RecyclerView meansRecyclerView) {
        this.meansRecyclerView = meansRecyclerView;
    }


    public void createListener(Activity activity) {
        try {
            //mi aspetto che il context sia come istanza una activity
            listMeanListener = (ListMeanListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ListMeanListener");
        }
    }

    private class ReadMeaningOfWord extends AsyncTask<String,Integer,ArrayList<String>>{
        private boolean isReadingMeaning = false;

        @Override
        protected void onPreExecute(){
            isReadingMeaning = true;
            final ProgressBar spinner = (ProgressBar) getActivity().findViewById(R.id.spinner);
            spinner.postDelayed(new Runnable() {
                public void run() {
                    if(isReadingMeaning) { //...bug.. da fare un lock alla variabile isReadingMeaning..
                        //se stai leggendo questo pezzo, l'onPostExecute si deve fermare..??..
                        spinner.setVisibility(View.VISIBLE);
                        spinner.setIndeterminate(true);
                    }
                }
            }, 500);

        }
        @Override
        protected ArrayList<String> doInBackground(String... paramsdictionaryAdvices) {
            alreadySearchedOnNetwork = true;
            DictionaryAdvice dictAdv = new DictionaryAdvice(paramsdictionaryAdvices[0],paramsdictionaryAdvices[1],paramsdictionaryAdvices[2]);
            return dictAdv.getMeanings();

        }

        @Override
        protected void onPostExecute(ArrayList<String> result) {
            //così non setta più l'animazione!
            isReadingMeaning = false;

            if(!result.isEmpty()) {
                showTheDialog("Scegli un significato: ", DialogType.SingleChoiceList, result);
            }else{
                alreadySearchedOnNetwork = false;
                showToast("meanings for word not found!");
            }
            ProgressBar spinner = (ProgressBar) getActivity().findViewById(R.id.spinner);
            spinner.setVisibility(View.INVISIBLE);
            spinner.setIndeterminate(false);
        }
    }

    private class SaveFileSound extends AsyncTask<File,Void,Boolean>{

        @Override
        protected Boolean doInBackground(File... files) {

            try {
                copyFile(files[0], files[1]);
            }catch(IOException ex){
                return false;
            }
            //se è riuscito a copiare il file:
            boolean isDeleted = files[0].delete();
            Log.w("Delete Check: ", "" + isDeleted);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result==false){
                showToast("sorry, there was an error to save the sound");
            }
        }

        private void copyFile(File src, File dst) throws IOException{
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dst);

            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) != -1) {
                out.write(buf, 0, len);
            }
            in.close();
            out.flush();
            out.close();

        }
    }



    private boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }
}
