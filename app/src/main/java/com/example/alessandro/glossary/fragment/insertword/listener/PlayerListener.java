package com.example.alessandro.glossary.fragment.insertword.listener;

import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.example.alessandro.glossary.R;

import java.io.File;
import java.io.IOException;

/**
 * Created by Alessandro on 04/11/2016.
 */
public class PlayerListener implements View.OnClickListener {
    private boolean startPlaying = false;
    private MediaPlayer mediaPlayer = null;
    private Context ctx;
    private String pathTmpSound;

    public PlayerListener(String pathTmpSound){
        this.pathTmpSound = pathTmpSound;
    }

    @Override
    public void onClick(View v) {
        if(!startPlaying) //se non sta registrando inizia
            startPlay((ImageButton) v);
        else //se sta registrando stoppa:
            stopPlay((ImageButton) v);
    }

    private void startPlay(final ImageButton btnPlayer) {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(pathTmpSound);
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mediaPlayer.start();
                    btnPlayer.setColorFilter(Color.parseColor("#1ABFBF"));
                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            stopPlay(btnPlayer);
                        }
                    });
                    startPlaying = true;
                }
            });

        } catch (IOException e) {
            Log.e("player", "prepare() failed");
        }

        Log.d("player: ", "start!");
    }

    private void stopPlay(ImageButton btnPlayer) {
        mediaPlayer.stop(); //necessario?
        mediaPlayer.release();
        btnPlayer.clearColorFilter();
        mediaPlayer = null;

        startPlaying = false;
        Log.d("player: ", "stop!");
    }
}
