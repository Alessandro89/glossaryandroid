package com.example.alessandro.glossary.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alessandro on 16/09/2016.
 */
public class Mean {
    private int id;
    private List<String> examples;
    private List<String> categories;


    private String mean;

    public Mean(){
        examples = new ArrayList<>();
        categories = new ArrayList<>();
    }

    public Mean(List<String> examples, List<String> category) {
        this.examples = examples;
        this.categories = category;
    }

    public List<String> getExamples() {
        return examples;
    }

    public String getMean() {
        return mean;
    }

    public void setMean(String mean) {
        this.mean = mean;
    }

    public void setExamples(List<String> examples) {
        this.examples = examples;
    }


    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
