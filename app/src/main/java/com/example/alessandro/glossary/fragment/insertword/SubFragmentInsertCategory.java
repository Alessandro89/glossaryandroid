package com.example.alessandro.glossary.fragment.insertword;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.alessandro.glossary.DataDialogFragment;
import com.example.alessandro.glossary.DialogType;
import com.example.alessandro.glossary.GlossaryActivity;
import com.example.alessandro.glossary.ModelDbWord;
import com.example.alessandro.glossary.R;
import com.example.alessandro.glossary.fragment.insertword.adapter.AdapterRecyclerInsertCategory;
import com.example.alessandro.glossary.fragment.insertword.listener.ListMeanListener;

import java.util.ArrayList;
import java.util.List;


public class SubFragmentInsertCategory extends Fragment implements DataDialogFragment.NoticeDialogListener, View.OnClickListener{

    private RecyclerView recyclerViewCategory;
    private ListMeanListener listMeanListener;

    private AdapterRecyclerInsertCategory categoryAdapterRecycler;
    private int position;
    private Activity myActivity;


    public SubFragmentInsertCategory() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_insertdatamean_category, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        if(getArguments()!=null){ //gli arguments rimangono anche dopo il destroyview.
            this.position = getArguments().getInt("positionOfList");
        }

        List<String> categoryList = listMeanListener.getListMeans().get(position).getCategories();
        categoryAdapterRecycler = new AdapterRecyclerInsertCategory((ArrayList<String>) categoryList, myActivity);

        recyclerViewCategory = (RecyclerView) myActivity.findViewById(R.id.recyclerViewCategory);
        recyclerViewCategory.setLayoutManager(new LinearLayoutManager(myActivity));
        recyclerViewCategory.setVerticalScrollBarEnabled(true);
        recyclerViewCategory.setAdapter(categoryAdapterRecycler);
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(this.getContext(),
                LinearLayoutManager.VERTICAL);
        recyclerViewCategory.addItemDecoration(mDividerItemDecoration);

        ImageButton btnAddCategory = (ImageButton) myActivity.findViewById(R.id.fabAddCategory);
        btnAddCategory.setOnClickListener(this);


    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);

        try {
            //mi aspetto che il context sia come istanza una activity
            this.myActivity = (Activity) context;

            listMeanListener = (ListMeanListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement ListMeanListener");
        }
    }
    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.fabAddCategory){

            showTheDialog("insert category");

        }
    }

    public void showTheDialog(String text){
        String tagOfThisFragment = this.getTag();
        //adesso il tag del fragment corrente si riferisce a questo fragment
        ((GlossaryActivity) myActivity).setTagCurrentSubFragment(tagOfThisFragment);
        DialogFragment dialog = new DataDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putString("testo", text);
        dialog.setArguments(bundle);

        ((DataDialogFragment) dialog).setDialogType(DialogType.Category);
        ((DataDialogFragment) dialog).setListCategories(new ModelDbWord(myActivity).getAllCategories(), categoryAdapterRecycler.getItems());

        dialog.show(myActivity.getFragmentManager(), "DataDialogFragment");

    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, ArrayList<String> listSelectedCategory, String singleChoice, DialogType typeDialog) {
        String data = ((EditText)dialog.getDialog().findViewById(R.id.textDialog)).getText().toString();

        switch (typeDialog){

            case Category:
                if(listSelectedCategory.size()>0) {
                    categoryAdapterRecycler.addAllItem(listSelectedCategory);
                    categoryAdapterRecycler.notifyDataSetChanged();
                    focusOnLastElementOnRecyclerView(recyclerViewCategory);
                }
                if(!data.equals("")) {
                    categoryAdapterRecycler.addItem(data);
                    categoryAdapterRecycler.notifyDataSetChanged();
                    focusOnLastElementOnRecyclerView(recyclerViewCategory);
                }
                updateCategoryListMean();
                break;
        }

        hideKeyboardFromEditText((EditText) dialog.getDialog().findViewById(R.id.textDialog));
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog, DialogType typeDialog) {
        hideKeyboardFromEditText((EditText) dialog.getDialog().findViewById(R.id.textDialog));
    }

    @Override
    public void onDismissClick(DialogType typeDialog) {

    }

    private void updateCategoryListMean() {
        ArrayList<String> categoryList = categoryAdapterRecycler.getItems();
        //imposto le categorie della lista in un certo elemento,
        //ho modificato così la lista (fa riferimento allo stesso oggetto)
        listMeanListener.getListMeans().get(position).setCategories(categoryList);
    }

    private void focusOnLastElementOnRecyclerView(RecyclerView lrecyclerView) {
        lrecyclerView.smoothScrollToPosition(lrecyclerView.getAdapter().getItemCount() - 1);
    }


    private void hideKeyboardFromEditText(EditText windowEdit){
        InputMethodManager imm = (InputMethodManager) myActivity.getSystemService(Context.INPUT_METHOD_SERVICE); //prendiamo l'input method manager
        imm.hideSoftInputFromWindow(windowEdit.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }



}