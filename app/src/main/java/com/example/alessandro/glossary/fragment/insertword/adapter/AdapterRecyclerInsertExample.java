package com.example.alessandro.glossary.fragment.insertword.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.alessandro.glossary.R;
import com.example.alessandro.glossary.fragment.AdapterRecycler;
import com.example.alessandro.glossary.fragment.insertword.listener.ListenerRecyclerDataMean;

import java.util.ArrayList;

/**
 * Created by Alessandro on 21/09/2016.
 */
public class AdapterRecyclerInsertExample extends RecyclerView.Adapter<AdapterRecyclerInsertExample.ViewHolder> implements AdapterRecycler<String> {
    private ArrayList<String> listExample;
    private Activity activity;
    @Override
    public ArrayList<String> getItems() {
        return listExample;
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView meanTextView;
        public ImageButton deleteButton;
        public ViewHolder(View v) {
            super(v);
            meanTextView = (TextView) v.findViewById(R.id.textViewMean);
            deleteButton = (ImageButton) v.findViewById(R.id.btnDeleteMean);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterRecyclerInsertExample(ArrayList<String> myDataset, Activity activity) {
        listExample = myDataset;
        this.activity = activity;
    }

    @Override
    public void addItem(String item){
        listExample.add(item);
    }

    @Override
    public void addAllItem(ArrayList<String> items) {
        listExample.addAll(items);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AdapterRecyclerInsertExample.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.itemlayout_recycleview_insertdataword, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.meanTextView.setText(listExample.get(position));
        final AdapterRecyclerInsertExample instance = this;

        //da cambiare qualcosa:
        ListenerRecyclerDataMean lrd = new ListenerRecyclerDataMean<String>(position,this);
        holder.deleteButton.setOnClickListener(lrd);
    }




    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return listExample.size();
    }

    public Activity getActivity() {
        return activity;
    }
}
