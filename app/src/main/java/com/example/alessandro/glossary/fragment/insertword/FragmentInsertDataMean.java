package com.example.alessandro.glossary.fragment.insertword;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.alessandro.glossary.R;


public class FragmentInsertDataMean extends Fragment implements View.OnClickListener{
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private String word;

    private int position;


    public FragmentInsertDataMean() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_insertdatamean, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FragmentActivity thisactivity = getActivity();
        ImageButton btnBack = (ImageButton) thisactivity.findViewById(R.id.backButton);

        btnBack.setOnClickListener(this);

        if(getArguments()!=null){ //gli arguments rimangono anche dopo il destroyview.
            this.position = getArguments().getInt("positionOfList");
            this.word = getArguments().getString("wordToInsert");
        }
        viewPager = (ViewPager) thisactivity.findViewById(R.id.pager);
        PagerAdapter pageradapter = new TabsPageAdapterMeanInsert(this.getChildFragmentManager(),thisactivity, position, word);
        viewPager.setAdapter(pageradapter);

        tabLayout = (TabLayout) thisactivity.findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);


    }

    @Override
    public void onStart(){
        super.onStart();
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.backButton){
            getActivity().onBackPressed();
        }

    }










}