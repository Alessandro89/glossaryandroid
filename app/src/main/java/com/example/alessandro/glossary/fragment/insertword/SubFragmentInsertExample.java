package com.example.alessandro.glossary.fragment.insertword;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.alessandro.glossary.DataDialogFragment;
import com.example.alessandro.glossary.DialogType;
import com.example.alessandro.glossary.DictionaryAdvice;
import com.example.alessandro.glossary.GlossaryActivity;
import com.example.alessandro.glossary.R;
import com.example.alessandro.glossary.fragment.insertword.adapter.AdapterRecyclerInsertExample;
import com.example.alessandro.glossary.fragment.insertword.listener.ListMeanListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class SubFragmentInsertExample extends Fragment implements  View.OnClickListener, DataDialogFragment.NoticeDialogListener{

    private RecyclerView recyclerViewExample;
    private ListMeanListener listMeanListener;
    private AdapterRecyclerInsertExample exampleAdapterRecycler;
    private int position;
    private Activity myActivity;
    private String word;
    private Toast currentToast;
    private boolean alreadySearchedOnNetwork;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_insertdatamean_example, container, false);
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(getArguments()!=null){ //gli arguments rimangono anche dopo il destroyview.
            //o viene passata o viene preso dall'activity..
            this.position = getArguments().getInt("positionOfList");
            this.word = getArguments().getString("wordToInsert");
        }

        List<String> exampleList = listMeanListener.getListMeans().get(position).getExamples();
        exampleAdapterRecycler = new AdapterRecyclerInsertExample((ArrayList<String>) exampleList, myActivity);
        recyclerViewExample = (RecyclerView) myActivity.findViewById(R.id.recyclerViewExample);
        recyclerViewExample.setLayoutManager(new LinearLayoutManager(myActivity));
        recyclerViewExample.setVerticalScrollBarEnabled(true);
        recyclerViewExample.setAdapter(exampleAdapterRecycler);

        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(this.getContext(),
                LinearLayoutManager.VERTICAL);
        recyclerViewExample.addItemDecoration(mDividerItemDecoration);

        ImageButton btnAddExample = (ImageButton) myActivity.findViewById(R.id.fabAddExample);
        ImageButton btnSuggestMean = (ImageButton) myActivity.findViewById(R.id.fabSuggestExample);

        btnAddExample.setOnClickListener(this);
        btnSuggestMean.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.fabAddExample){
            showTheDialog("insert example",DialogType.Example,null);
        }
        if(v.getId() == R.id.fabSuggestExample){
            if(isConnected()) {
                if(!alreadySearchedOnNetwork) {//solo un click per volta
                    String[] translationFromTo = ((GlossaryActivity) getActivity()).getCurrentTranslation().getDescription().split("-");
                    new ReadExamplesOfWord().execute(translationFromTo[0], translationFromTo[1], word);
                }
            }
            else{
                showToast("you are not connected!");
            }
        }
    }

    public void showTheDialog(String text, DialogType dialogType, ArrayList<String> examples){
        String tagOfThisFragment = this.getTag();
        //adesso il tag del fragment corrente si riferisce a questo fragment
        ((GlossaryActivity) myActivity).setTagCurrentSubFragment(tagOfThisFragment);

        DialogFragment dialog = new DataDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putString("testo", text);
        dialog.setArguments(bundle);

        ((DataDialogFragment) dialog).setDialogType(dialogType);

        if(dialogType == DialogType.SingleChoiceList){
            ((DataDialogFragment)dialog).setListSingleChoice(examples);
        }

        dialog.show(myActivity.getFragmentManager(), "DataDialogFragment");


    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);

        try {
            //mi aspetto che il context sia come istanza una activity
            this.myActivity = (Activity) context;
            listMeanListener = (ListMeanListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement ListMeanListener");
        }
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, ArrayList<String> listSelectedCategory, String singleChoice, DialogType typeDialog) {
        String data = null;
        if(typeDialog!=DialogType.SingleChoiceList)
            data = ((EditText)dialog.getDialog().findViewById(R.id.textDialog)).getText().toString();

        switch (typeDialog){
            case Example:
                addExample(data);
                break;
            case SingleChoiceList:
                addExample(singleChoice);

        }

        hideKeyboardFromEditText((EditText) dialog.getDialog().findViewById(R.id.textDialog));
    }

    private void addExample(String data) {
        if(!data.equals("")){
            exampleAdapterRecycler.addItem(data);
            exampleAdapterRecycler.notifyDataSetChanged();
            updateExamplesListMean();
            focusOnLastElementOnRecyclerView(recyclerViewExample);
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog, DialogType typeDialog) {
        hideKeyboardFromEditText((EditText) dialog.getDialog().findViewById(R.id.textDialog));
    }

    @Override
    public void onDismissClick(DialogType typeDialog) {
        if(typeDialog==DialogType.SingleChoiceList)
            alreadySearchedOnNetwork = false;
    }


    private void updateExamplesListMean(){
        ArrayList<String> exampleList = exampleAdapterRecycler.getItems();
        listMeanListener.getListMeans().get(position).setExamples(exampleList);
    }


    private void hideKeyboardFromEditText(EditText windowEdit){
        if(windowEdit!=null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE); //prendiamo l'input method manager
            imm.hideSoftInputFromWindow(windowEdit.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
        }
    }

    private void focusOnLastElementOnRecyclerView(RecyclerView lrecyclerView) {
        lrecyclerView.smoothScrollToPosition(lrecyclerView.getAdapter().getItemCount() - 1);
    }

    private class ReadExamplesOfWord extends AsyncTask<String,Void,ArrayList<String>> {
        private boolean isReadingExample = false;

        @Override
        protected void onPreExecute(){
            isReadingExample = true;
            final ProgressBar spinner = (ProgressBar) getActivity().findViewById(R.id.spinner);
            spinner.postDelayed(new Runnable() {
                public void run() {
                    if(isReadingExample) { //..da fare un lock..
                        spinner.setVisibility(View.VISIBLE);
                        spinner.setIndeterminate(true);
                    }
                }
            }, 500);

        }
        @Override
        protected ArrayList<String> doInBackground(String... paramsdictionaryAdvices) {

            alreadySearchedOnNetwork = true;
            DictionaryAdvice dictAdv = new DictionaryAdvice(paramsdictionaryAdvices[0],paramsdictionaryAdvices[1],paramsdictionaryAdvices[2]);
            return dictAdv.getExamples();
        }

        @Override
        protected void onPostExecute(ArrayList<String> result) {
            isReadingExample = false;

            if(!result.isEmpty()) {
                showTheDialog("scegli un esempio dalla lista:", DialogType.SingleChoiceList, result);
            }else{
                alreadySearchedOnNetwork = false;
                showToast("examples for word not found!");
            }
            ProgressBar spinner = (ProgressBar) getActivity().findViewById(R.id.spinner);
            spinner.setVisibility(View.INVISIBLE);
            spinner.setIndeterminate(false);
        }
    }

    private boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    private void showToast(String message) {
        if(currentToast!=null)
            currentToast.cancel();
        currentToast = Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT);
        currentToast.setGravity(Gravity.CENTER, 0, 0);
        currentToast.show();
    }
}