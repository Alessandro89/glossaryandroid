package com.example.alessandro.glossary.fragment;

import android.app.Activity;

import java.util.ArrayList;

/**
 * Created by Alessandro on 23/09/2016.
 */
public interface AdapterRecycler<T> {
    public ArrayList<T> getItems();
    public void addItem(T item);
    public void addAllItem(ArrayList<T> item);
    public Activity getActivity();

}
