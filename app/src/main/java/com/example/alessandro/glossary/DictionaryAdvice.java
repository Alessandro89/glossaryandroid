package com.example.alessandro.glossary;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Alessandro on 28/10/2016.
 */
public class DictionaryAdvice {

    private String baseUrl = "https://glosbe.com/gapi/";
    private String queryforTranslation;
    private String queryforExample;
    private String from;
    private String dest;
    private final int TIME_OUT_MILLIS = 7000;

    public DictionaryAdvice(String from,String dest,String word){
        this.from = from;
        this.dest = dest;
        queryforTranslation = "translate?from="+from+"&dest="+dest+"&format=json&phrase="+word;
        queryforExample = "tm?from="+from+"&dest="+dest+"&format=json&phrase="+word;
    }

    public ArrayList<String> getMeanings() {
        ArrayList<String> meaningList = new ArrayList<>();
        HttpURLConnection URLConnectionTranslation = null;
        try {
            URL urlTranslation = new URL(baseUrl + queryforTranslation);
            URLConnectionTranslation = (HttpURLConnection) urlTranslation.openConnection();
            URLConnectionTranslation.setRequestMethod("GET");
            URLConnectionTranslation.setConnectTimeout(TIME_OUT_MILLIS);
            URLConnectionTranslation.setReadTimeout(TIME_OUT_MILLIS);
            URLConnectionTranslation.setDoInput(true);

            String jsonResponseTranslation = getJsonResponse(URLConnectionTranslation);
            //abbiamo tuc che è un array di oggetti,
            //dove ogni elemento contiene meaning che è un array di oggetti
            //dobbiamo prendere ogni meaning dove il language è uguale a dest
            JsonParser jsonParser = new JsonParser();
            JsonElement elementJson = jsonParser.parse(jsonResponseTranslation);
            JsonObject jsonObject = elementJson.getAsJsonObject();
            JsonArray jsonArrayTuc = jsonObject.getAsJsonArray("tuc");
            for (int i = 0; i < jsonArrayTuc.size(); i++) {
                JsonArray jsonArrayMeaning = jsonArrayTuc.get(i).getAsJsonObject().getAsJsonArray("meanings");
                if (jsonArrayMeaning != null) {
                    for (int j = 0; j < jsonArrayMeaning.size(); j++) {
                        JsonObject jsonObjectMeaning = jsonArrayMeaning.get(j).getAsJsonObject();
                        //if (jsonObjectMeaning.get("language").getAsString().equals("it")) {
                        meaningList.add(jsonObjectMeaning.get("text").getAsString());
                        //}
                    }
                }
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (URLConnectionTranslation != null)
                URLConnectionTranslation.disconnect();
        }

        return meaningList;
    }

    public ArrayList<String> getExamples() {
        ArrayList<String> exampleList = new ArrayList<>();
        HttpURLConnection URLConnectionExample = null;
        try {
            URL urlExample = new URL(baseUrl + queryforExample);
            URLConnectionExample = (HttpURLConnection) urlExample.openConnection();
            URLConnectionExample.setRequestMethod("GET");
            URLConnectionExample.setConnectTimeout(TIME_OUT_MILLIS);
            URLConnectionExample.setReadTimeout(TIME_OUT_MILLIS);
            String jsonResponseExample = getJsonResponse(URLConnectionExample);
            JsonParser jsonParser = new JsonParser();
            JsonElement elementJson = jsonParser.parse(jsonResponseExample);
            JsonObject jsonObject = elementJson.getAsJsonObject();
            JsonArray jsonArrayExamples = jsonObject.getAsJsonArray("examples");
            for(int i=0;i<jsonArrayExamples.size();i++){
                exampleList.add(jsonArrayExamples.get(i).getAsJsonObject().get("first").getAsString());
                exampleList.add(jsonArrayExamples.get(i).getAsJsonObject().get("second").getAsString());
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(URLConnectionExample!=null)
                URLConnectionExample.disconnect();
        }
        return exampleList;
    }

    private String getJsonResponse(HttpURLConnection urlConnection) throws IOException {
        BufferedReader in = null;
        StringBuffer response = new StringBuffer();
        try {
             in = new BufferedReader(
                    new InputStreamReader(urlConnection.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        }catch (IOException e){
            throw e; //propaghiamo l'eccezione
        }finally{
            if(in!=null)
            //se per caso abbiamo un eccezione, chiudiamo prima l'inputStream
            in.close();
        }

        return response.toString();
    }
}
