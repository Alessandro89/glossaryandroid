package com.example.alessandro.glossary.fragment.searchwords;

/**
 * Created by Alessandro on 13/10/2016.
 */
public class CategoryChecked {
    private String category;
    private boolean checked;

    public CategoryChecked(String category, boolean checked) {
        this.category = category;
        this.checked = checked;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
