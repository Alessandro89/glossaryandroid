package com.example.alessandro.glossary.fragment.showwords;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.alessandro.glossary.R;
import com.example.alessandro.glossary.fragment.AdapterRecycler;
import com.example.alessandro.glossary.fragment.insertword.listener.PlayerListener;
import com.example.alessandro.glossary.fragment.listener.ListenerRecyclerClickItem;
import com.example.alessandro.glossary.model.Word;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Alessandro on 21/09/2016.
 */
public class AdapterRecyclerDailyWords extends RecyclerView.Adapter<AdapterRecyclerDailyWords.ViewHolder> implements AdapterRecycler<Word> {
    private ArrayList<Word> listWord;

    private Activity activity;
    @Override
    public ArrayList<Word> getItems() {
        return listWord;
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView wordTextView;
        public TextView wordTextDate;
        public ImageButton btnSoundOff;
        public ImageButton btnSoundOn;

        public ViewHolder(View v) {
            super(v);
            wordTextView = (TextView) v.findViewById(R.id.textViewDailyWord);
            wordTextDate = (TextView) v.findViewById(R.id.textViewDateDailyWord);
            btnSoundOff = (ImageButton) v.findViewById(R.id.btnSoundOff);
            btnSoundOn = (ImageButton) v.findViewById(R.id.btnSound);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterRecyclerDailyWords(ArrayList<Word> listWord, Activity activity) {
        this.listWord = listWord;
        this.activity = activity;
    }

    @Override
    public void addItem(Word word){
        listWord.add(word);
    }

    @Override
    public void addAllItem(ArrayList<Word> listWord){
        this.listWord.addAll(listWord);
    }

    @Override
    public AdapterRecyclerDailyWords.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.itemlayout_recycleview_showdailyword, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.wordTextView.setText(listWord.get(position).getName());

        //se abbiamo il suono:
        if(listWord.get(position).getSoundPathWord()!=null){
            holder.btnSoundOff.setVisibility(View.INVISIBLE);
            holder.btnSoundOn.setVisibility(View.VISIBLE);
            holder.btnSoundOn.setOnClickListener(new PlayerListener(listWord.get(position).getSoundPathWord()));
        }
        else{
            holder.btnSoundOn.setVisibility(View.INVISIBLE);
            holder.btnSoundOff.setVisibility(View.VISIBLE);
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String formatDate = sdf.format(new Date(listWord.get(position).getTimeMillisecondDate()));
        holder.wordTextDate.setText(formatDate);

        Bundle b = new Bundle();

        b.putInt("idWord", listWord.get(position).getId());
        b.putString("nameWord",listWord.get(position).getName());
        ListenerRecyclerClickItem listShowWord = new ListenerRecyclerClickItem<Word>(position,this,new FragmentShowMeans(),b);
        holder.itemView.setOnClickListener(listShowWord);
    }




    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return listWord.size();
    }

    public Activity getActivity() {
        return activity;
    }
}
