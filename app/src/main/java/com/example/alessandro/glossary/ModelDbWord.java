package com.example.alessandro.glossary;

import android.content.Context;

import com.example.alessandro.database.HelperDbGlossary;
import com.example.alessandro.exception.MyExceptionInsert;
import com.example.alessandro.glossary.model.Mean;
import com.example.alessandro.glossary.model.Translation;
import com.example.alessandro.glossary.model.Word;

import java.util.ArrayList;

/**
 * Created by Alessandro on 20/07/2016.
 */
public class ModelDbWord implements IModelWord {

    private HelperDbGlossary helpdb;

    public ModelDbWord(Context context) {
        helpdb = new HelperDbGlossary(context);
    }

    @Override
    public void insertWord(Word wd) throws MyExceptionInsert {
        helpdb.insertWord(wd);
    }

    @Override
    public Word lightGetWordByName(String word) {
        return helpdb.lightGetWordByName(word); //not yet implemented
    }

    @Override
    public ArrayList<Word> lightGetAllDailyWords(int idTranslation) {
        return helpdb.lightGetAllDailyWords(idTranslation);
    }

    @Override
    public ArrayList<Mean> lightGetAllMeansByIdWord(int idWord) {
        return helpdb.lightGetAllMeansByIdWord(idWord);
    }

    @Override
       public ArrayList<String> getAllExampleByIdMean(int idMean) {
        return helpdb.getAllExampleByIdMean(idMean);
    }


    @Override
    public ArrayList<String> getAllCategoryByIdMean(int idMean) {
        return helpdb.getAllCategoryByIdMean(idMean);
    }


    @Override
    public ArrayList<String> getAllCategories() {
        return helpdb.getAllCategories();
    }

    @Override
    public ArrayList<Translation> getAllTranslations() {
        return helpdb.getAllTranslations();
    }


    @Override
    public ArrayList<Word> getWordsByCategories(int idTranslation, ArrayList<String> listCategory) {
        return helpdb.getWordsByCategories(idTranslation, listCategory);
    }

    public void deletedatabaseForTest(){
        helpdb.deleteDatabaseForTest();
    }
}