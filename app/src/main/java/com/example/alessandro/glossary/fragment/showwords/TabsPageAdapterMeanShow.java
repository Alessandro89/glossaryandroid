package com.example.alessandro.glossary.fragment.showwords;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.alessandro.glossary.R;
import com.example.alessandro.glossary.fragment.insertword.SubFragmentInsertCategory;
import com.example.alessandro.glossary.fragment.insertword.SubFragmentInsertExample;

/**
 * Created by Alessandro on 27/09/2016.
 */
public class TabsPageAdapterMeanShow extends FragmentPagerAdapter{

    private Context ctx;
    private int idMean;

    public TabsPageAdapterMeanShow(FragmentManager fm, Context ctx, int idMean) {
        super(fm);
        this.ctx = ctx;
        this.idMean = idMean;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle b = new Bundle();
        b.putInt("idMean", idMean);
        switch (position){
            case 0:
                Fragment fExample = new SubFragmentShowExample();
                fExample.setArguments(b);
                return fExample;
            case 1:
                Fragment fCategory = new SubFragmentShowCategory();
                fCategory.setArguments(b);
                return fCategory;
            default:
                return null;
        }
    }


    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position){
        switch(position){
            case 0:
                return ctx.getString(R.string.example);
            case 1:
                return ctx.getString(R.string.category);

            default:
                return null;
        }
    }
}

