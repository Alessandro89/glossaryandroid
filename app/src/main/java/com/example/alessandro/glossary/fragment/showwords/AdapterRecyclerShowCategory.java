package com.example.alessandro.glossary.fragment.showwords;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alessandro.glossary.R;
import com.example.alessandro.glossary.fragment.AdapterRecycler;

import java.util.ArrayList;

/**
 * Created by Alessandro on 21/09/2016.
 */
public class AdapterRecyclerShowCategory extends RecyclerView.Adapter<AdapterRecyclerShowCategory.ViewHolder> implements AdapterRecycler<String> {
    private ArrayList<String> listCategory;
    private Activity activity;
    @Override
    public ArrayList<String> getItems() {
        return listCategory;
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView categoryTextView;
        public ViewHolder(View v) {
            super(v);
            categoryTextView = (TextView) v.findViewById(R.id.textViewShowData);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterRecyclerShowCategory(ArrayList<String> listCategory, Activity activity) {
        this.listCategory = listCategory;
        this.activity = activity;
    }


    // Create new views (invoked by the layout manager)
    @Override
    public AdapterRecyclerShowCategory.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.itemlayout_recycleview_showdata, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.categoryTextView.setText(listCategory.get(position));

    }

    @Override
    public void addItem(String item){
        listCategory.add(item);
    }

    @Override
    public void addAllItem(ArrayList<String> items) {
        listCategory.addAll(items);
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return listCategory.size();
    }

    public Activity getActivity() {
        return activity;
    }
}
