package com.example.alessandro.glossary.fragment.insertword.listener;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.alessandro.glossary.GlossaryActivity;
import com.example.alessandro.glossary.R;
import com.example.alessandro.glossary.fragment.AdapterRecycler;
import com.example.alessandro.glossary.fragment.insertword.adapter.AdapterRecyclerInsertMean;
import com.example.alessandro.glossary.fragment.insertword.FragmentInsertDataMean;

import org.w3c.dom.Text;

/**
 * Created by Alessandro on 23/09/2016.
 */
public class ListenerRecyclerMean implements View.OnClickListener{

    private int positionItem;
    private EditText wordView;
    private AdapterRecycler adapterRecycler;

    public ListenerRecyclerMean(int positionItem, EditText wordView, AdapterRecycler adapterRecycler) {
        this.positionItem = positionItem;
        this.adapterRecycler = adapterRecycler;
        this.wordView = wordView;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnDeleteMean) {
            ((AdapterRecyclerInsertMean) adapterRecycler).getItems().remove(positionItem);
            ((AdapterRecyclerInsertMean) adapterRecycler).notifyDataSetChanged();
        }
        else{ //non va bene l'else
            String s = "";
            Log.d("item is clicked: " , "" +  this.positionItem);
            changeFragment();
        }

    }

    private void changeFragment() {
        Activity thisactivity = ((AdapterRecyclerInsertMean) adapterRecycler).getActivity();
        FragmentManager fm = ((FragmentActivity) (thisactivity)).getSupportFragmentManager(); //getFragmentManager();
        //l'unica activity in cui viene passata è il glossary activity, per cui nessun problema..
        //final Fragment initialFragment = fm.findFragmentByTag(((GlossaryActivity) thisactivity).getTagCurrentFragment());
        //final Fragment f = new FragmentInsertDataMean();
        Fragment f = new FragmentInsertDataMean();
        Bundle bundleArguments = new Bundle();
        bundleArguments.putInt("positionOfList", positionItem);
        bundleArguments.putString("wordToInsert",wordView.getText().toString());
        f.setArguments(bundleArguments);
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.containerfragment, f, ((GlossaryActivity) thisactivity).getTagCurrentFragment());

        ft.addToBackStack(null);

        ft.commit();
    }
}
