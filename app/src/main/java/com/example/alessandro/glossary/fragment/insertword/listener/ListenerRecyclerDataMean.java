package com.example.alessandro.glossary.fragment.insertword.listener;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.alessandro.glossary.R;
import com.example.alessandro.glossary.fragment.AdapterRecycler;

/**
 * Created by Alessandro on 23/09/2016.
 */
public class ListenerRecyclerDataMean<T> implements View.OnClickListener{

    private int positionItem;
    private AdapterRecycler<T> adapterRecycler;

    public ListenerRecyclerDataMean(int positionItem, AdapterRecycler<T> adapterRecycler) {
        this.positionItem = positionItem;
        this.adapterRecycler = adapterRecycler;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnDeleteMean) {
            adapterRecycler.getItems().remove(positionItem);
            //?? sto dicendo.. un tipo di recyclerView.Adapter<...> che è la superclasse.. e che tale classe ha come
            //holder un oggetto di tipo RecyclerView.ViewHolder, alla fine tutti ce l'hanno perchè la estendono, chiama
            //il notifyDataSetChanged (che è presente sulla superclasse)
            ((RecyclerView.Adapter<RecyclerView.ViewHolder>) adapterRecycler).notifyDataSetChanged();

        }

    }
}
