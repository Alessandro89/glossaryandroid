package com.example.alessandro.glossary;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.BackStackEntry;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.alessandro.exception.NotImplementedException;
import com.example.alessandro.glossary.fragment.insertword.FragmentInsertWord;
import com.example.alessandro.glossary.fragment.searchwords.FragmentSearchWord;
import com.example.alessandro.glossary.fragment.insertword.listener.ListMeanListener;
import com.example.alessandro.glossary.fragment.showwords.FragmentShowDailyWord;
import com.example.alessandro.glossary.model.Mean;
import com.example.alessandro.glossary.model.Translation;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class GlossaryActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, ListMeanListener {

    private Translation currentTranslation;
    private ArrayList<Mean> listMeans = new ArrayList<Mean>();
    private String tagCurrentFragment = "currentFragment";
    private String tagCurrentSubFragment = "";

    //private HashMap hmpPosFrag = new HashMap<Integer,Fragment>(); occupa memoria!
    private int currentPositionForFragment = 0;
    private Toolbar myToolbar;
    private static final String title = "Glossary";
    private PopupMenu popupTranslation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_glossary);

        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setNavigationIcon(R.drawable.ic_menu_black_18dp);
        myToolbar.setNavigationOnClickListener(this);

        String[] menu = getResources().getStringArray(R.array.menu);
        String[] menuForBar = getResources().getStringArray(R.array.menuForBar);
        ListView drawerList = (ListView) findViewById(R.id.left_drawer);
        // Set the adapter for il drawer
        ArrayAdapter<String> arrayAdapter =  new MyArrayAdapter(this, menu);

        drawerList.setAdapter(arrayAdapter);

        drawerList.setOnItemClickListener(this);
        //myToolbar.setTitle(title);

        //vedo se il fragment e' gia presente
        Fragment f = getSupportFragmentManager().findFragmentByTag(this.getTagCurrentFragment());

        //da capire qual'è il current, giri lo schermo e allora qual'è?  (da gestire)
        if(savedInstanceState!=null){
            currentPositionForFragment = savedInstanceState.getInt("currentPositionFragment");
            restoreListMeans(savedInstanceState);
        }

        //se non e' presente lo inserisco
        if(f==null) {
            //NON lo vedo necesario, le view vengono salvate chiamando super.onSavedInstanceState.. così
            //il fragment rimane attaccato.. quando lo chiami la prima volta il fragment è null
            //qui fai new InsterFragmetn().. il "fragment home" d'avvio..
            Fragment fCurrent = (Fragment) getFragmentByPosition(currentPositionForFragment);

            //lo aggiungiamo, il fragment container non ha un fragment... va aggiunto dinamicamente
            getSupportFragmentManager().beginTransaction().add(R.id.containerfragment, fCurrent, this.tagCurrentFragment).commit();
            // Set the list's click listener
            //drawerList.setOnItemClickListener(new DrawerItemClickListener());
             //other fragments will be added later
        }

        getSupportActionBar().setTitle(title + " - " + menuForBar[currentPositionForFragment]);

        ModelDbWord modelDbWord = new ModelDbWord(getApplicationContext());
        List<Translation> listTranslation = modelDbWord.getAllTranslations();
        if(savedInstanceState!=null){
            int idTranslationCurrent = savedInstanceState.getInt("currentTranslationId");
            for(Translation tr: listTranslation) {
                if(tr.getIdTranslation() == idTranslationCurrent) {
                    currentTranslation = tr;
                }
            }
        }
        else {
            currentTranslation = listTranslation.get(0);
        }
        getSupportActionBar().setSubtitle(currentTranslation.getDescription());

    }


    private void setUpTranslationPopUp() {
        ModelDbWord modelDbWord = new ModelDbWord(getApplicationContext());
        List<Translation> listTranslation = modelDbWord.getAllTranslations();
        //http://stackoverflow.com/questions/14729592/show-popup-menu-on-actionbar-item-click
        View itemPopUp = findViewById(R.id.actionChoiceDictionary);
        popupTranslation = new PopupMenu(this,itemPopUp);

        for(Translation tr: listTranslation) {
            popupTranslation.getMenu().add(0, tr.getIdTranslation(),0,tr.getDescription());
            popupTranslation.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    Translation tr = new Translation();
                    tr.setDescription(item.getTitle().toString());
                    tr.setIdTranslation(item.getItemId());
                    //traduzione corrente corrente
                    int precedentIdTranslation = getCurrentTranslation().getIdTranslation();
                    //nuova traduzione
                    setCurrentTranslation(tr);
                    //se la traduzione è diversa aggiorniamo il fragment se è necessario
                    updateFragmentIfNeeded(precedentIdTranslation);
                    return true;

                }
            });
        }
    }

    private void updateFragmentIfNeeded(int precedentIdTranslation) {
        //se è una nuova traduzione
        if(precedentIdTranslation!=this.currentTranslation.getIdTranslation()){

            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment currentFragment = fragmentManager.findFragmentByTag(this.getTagCurrentFragment());

            //se il fragment corrente è un instanza del fragment show daily word.. lo ricarichiamo..
            // e nella ricerca no..andrebbe corretto
            if(currentFragment instanceof FragmentShowDailyWord){
                //refresh fragment!
                if(fragmentManager.getBackStackEntryCount()>0) {
                    BackStackEntry firstBackStack = fragmentManager.getBackStackEntryAt(0);
                    if (firstBackStack != null) {
                        fragmentManager.popBackStackImmediate(firstBackStack.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE); //rimuove anche il primo con inclusives
                    }
                }

                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                /** Detach the given fragment from the UI.  This is the same state as
                * when it is put on the back stack: the fragment is removed from
                        * the UI, however its state is still being actively managed by the
                        * fragment manager.When going into this state its view hierarchy
                * is destroyed.*/
                fragmentTransaction.detach(currentFragment);
                /**
                 * Re-attach a fragment after it had previously been deatched from
                 * the UI with {@link #detach(Fragment)}.  This
                 * causes its view hierarchy to be re-created, attached to the UI,
                 * and displayed.*/
                fragmentTransaction.attach(currentFragment);
                fragmentTransaction.commit();
            }
        }

    }

    //Initialize the contents of the Activity's standard options menu. You should place your menu items in to menu.
    //This is only called once, the first time the options menu is displayed.

    //qui creaimo il menu cambio lingua
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionsactionbar, menu); //..
        return super.onCreateOptionsMenu(menu); //..

    }


    private void restoreListMeans(Bundle savedInstanceState) {
        String gsonListMean = savedInstanceState.getString("jsonListMean");
        Type listMeanType = new TypeToken<ArrayList<Mean>>() {}.getType();

        this.listMeans = new Gson().fromJson(gsonListMean, listMeanType);
    }

    public void setTagCurrentFragment(String tagCurrentFragment) {
        this.tagCurrentFragment = tagCurrentFragment;
    }

    private Fragment getFragmentByPosition(int currentPosition){
        switch(currentPosition){
            case 0:
                return new FragmentInsertWord();
            case 1:
                return new FragmentSearchWord();
            case 2:
                return new FragmentShowDailyWord();
            default:
                return null;
        }
    }

    @Override
    public void onClick(View v){
        //questo on click serve per mostrare il drawer, andrebbe messo il controllo della view!

        //if(v.getId() == R.id.my_toolbar){ //non viene passata la view della toolbar, quale allora? la view della icona
       showNavigationDrawer();
      //  }
    }


    //viene mostrato il drawer
    private void showNavigationDrawer(){
        DrawerLayout dl = (DrawerLayout) findViewById(R.id.drawer_layout2);
        //dl.openDrawer(findViewById(R.id.drawer_layout2));
        dl.openDrawer(Gravity.LEFT);
    }

    //clicchi un elemento del drawer:
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //che item ha clickato?, lo vediamo dalla posizione
        String[] menu = getResources().getStringArray(R.array.menu);
        String[] menuForBar = getResources().getStringArray(R.array.menuForBar);

        Log.d("item clicked: ", menu[position]);
        currentPositionForFragment = position;
        //in base all'item cliccato apro il fragment giusto..
        Fragment f = (Fragment) getFragmentByPosition(position);
        if(f!=null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            //rimuoviamo tutti i fragment che sono nel backstack, in maniera tale da avere un backstack vuoto
            // altriemnti se aprimao un nuovo fragment, quando vai indietro ritorni al fragment precedente
            // azzeriamo quindi il backstack
            //se c'è qualcosa nel backstack:
            if(fragmentManager.getBackStackEntryCount()>0) {
                BackStackEntry firstBackStack = fragmentManager.getBackStackEntryAt(0);
                if (firstBackStack != null) {
                    //rimuovi tutti i framgnet presenti nel backstack
                    fragmentManager.popBackStackImmediate(firstBackStack.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE); //rimuove anche il primo con inclusives
                }
            }
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            //fragmentTransaction.remove(getSupportFragmentManager().findFragmentById(R.id.fragmentSection));
            //sostituisci il fragment presente con quello nuovo, preso in base all'item cliccato (getSupportFragmentManager)
            fragmentTransaction.replace(R.id.containerfragment, f, this.getTagCurrentFragment());

            fragmentTransaction.commit();
            //mettiamo il titolo giusto nel action bar
            getSupportActionBar().setTitle(title + " - " + menuForBar[currentPositionForFragment]);

        }else{
            try {
                //stai cliccando un elemento del menu non implementato.. (non succede..)
                throw new NotImplementedException("fragment non implementato!");
            } catch (NotImplementedException e) {
                e.printStackTrace();
            }
        }

    }

    //viene chiamato quando clicchi il cambio lingua
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(popupTranslation==null) //..
            setUpTranslationPopUp(); //se non è stato definito il popup, lo settiamo

        switch (item.getItemId()){
            case R.id.actionChoiceDictionary:
                popupTranslation.show(); //mostriamo il popup
                return true;
            default:
                //se sei qui, non è stata riconosciuta l'azione..
                //invoke the superclass to handle it
                return super.onOptionsItemSelected(item);
        }
    }

    public String getTagCurrentFragment() {
        return tagCurrentFragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("currentPositionFragment", currentPositionForFragment);
        outState.putInt("currentTranslationId", this.currentTranslation.getIdTranslation());

        //da usare un "Parcelable.."
        Gson gson = new Gson();
        String listMeanJson = gson.toJson(this.listMeans);
        outState.putString("jsonListMean",listMeanJson);
        super.onSaveInstanceState(outState);
    }

    public String getTagCurrentSubFragment() {
        return tagCurrentSubFragment;
    }

    public void setTagCurrentSubFragment(String tagCurrentSubFragment) {
        this.tagCurrentSubFragment = tagCurrentSubFragment;
    }

    @Override
    public void setListMeans(ArrayList<Mean> listMeans) {
        this.listMeans = listMeans;
    }

    @Override
    public ArrayList<Mean> getListMeans() {
        return listMeans;
    }

    public Translation getCurrentTranslation() {
        return currentTranslation;
    }

    public void setCurrentTranslation(Translation currentTranslation) {
        this.currentTranslation = currentTranslation;
        getSupportActionBar().setSubtitle(currentTranslation.getDescription());

    }

    //array adapter per il drawer
    public class MyArrayAdapter extends ArrayAdapter<String>{
        public MyArrayAdapter(Context context, String[] menu) {
            super(context, 0, menu);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            String item  = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_array_menu, parent, false);
            }
            // Lookup view for data population
            TextView menuItem = (TextView) convertView.findViewById(R.id.textViewMenu);
            // Populate the data into the template view using the data object
            menuItem.setText(item);

            return convertView;
        }
    }


}
