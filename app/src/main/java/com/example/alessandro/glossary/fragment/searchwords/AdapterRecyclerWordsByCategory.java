package com.example.alessandro.glossary.fragment.searchwords;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.alessandro.glossary.R;
import com.example.alessandro.glossary.fragment.AdapterRecycler;
import com.example.alessandro.glossary.fragment.insertword.listener.PlayerListener;
import com.example.alessandro.glossary.fragment.listener.ListenerRecyclerClickItem;
import com.example.alessandro.glossary.model.Word;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by Alessandro on 21/09/2016.
 */
public class AdapterRecyclerWordsByCategory extends RecyclerView.Adapter<AdapterRecyclerWordsByCategory.ViewHolder> implements AdapterRecycler<Word> {
    private ArrayList<Word> listWordByCategory;

    private Activity activity;
    @Override
    public ArrayList<Word> getItems() {
        return listWordByCategory;
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView wordTextViewWord;
        public ImageButton btnSoundOn;
        public ImageButton btnSoundOff;

        public ViewHolder(View v) {
            super(v);
            wordTextViewWord = (TextView) v.findViewById(R.id.textViewShowWord);
            btnSoundOff = (ImageButton) v.findViewById(R.id.btnSoundOff);
            btnSoundOn = (ImageButton) v.findViewById(R.id.btnSound);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterRecyclerWordsByCategory(ArrayList<Word> listWordByCategory, Activity activity) {
        this.listWordByCategory = listWordByCategory;
        this.activity = activity;
    }

    @Override
    public void addItem(Word word){
        listWordByCategory.add(word);
    }

    @Override
    public void addAllItem(ArrayList<Word> listWord){
        this.listWordByCategory.addAll(listWord);
    }

    @Override
    public AdapterRecyclerWordsByCategory.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.itemlayout_recycleview_showword, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.wordTextViewWord.setText(listWordByCategory.get(position).getName());
        //se abbiamo il suono:
        if(listWordByCategory.get(position).getSoundPathWord()!=null){
            holder.btnSoundOff.setVisibility(View.INVISIBLE);
            holder.btnSoundOn.setVisibility(View.VISIBLE);
            holder.btnSoundOn.setOnClickListener(new PlayerListener(listWordByCategory.get(position).getSoundPathWord()));
        }
        else{
            holder.btnSoundOn.setVisibility(View.INVISIBLE);
            holder.btnSoundOff.setVisibility(View.VISIBLE);
        }

        Bundle b = new Bundle();
        Gson gson = new Gson();
        //devo dare solo i significati estratti di certe categorie, non tutti i significati!:
        String gsonMeans = gson.toJson(listWordByCategory.get(position).getMeans());
        //gli devo passare la lista dei Mean della parola.. la lista dei mean sarà già solo quella di certe categorie..
        b.putString("jsonMeansByCategory",gsonMeans);
        b.putString("nameWord",listWordByCategory.get(position).getName());
        ListenerRecyclerClickItem listShowMeansWordofCategory = new ListenerRecyclerClickItem<Word>(position,this,new FragmentShowMeansByWordCategory(),b);
        holder.itemView.setOnClickListener(listShowMeansWordofCategory);
    }




    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return listWordByCategory.size();
    }

    public Activity getActivity() {
        return activity;
    }
}
