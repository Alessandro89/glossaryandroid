package com.example.alessandro.glossary;

import com.example.alessandro.exception.MyExceptionInsert;
import com.example.alessandro.glossary.model.Mean;
import com.example.alessandro.glossary.model.Translation;
import com.example.alessandro.glossary.model.Word;

import java.util.ArrayList;

/**
 * Created by Alessandro on 18/07/2016.
 */
public interface IModelWord {
    public void insertWord(Word c) throws MyExceptionInsert;
    //light-> do solo quello che serve non l'oggetto completo
    public Word lightGetWordByName(String word);
    public ArrayList<Word> lightGetAllDailyWords(int idTranslation);
    public ArrayList<Mean> lightGetAllMeansByIdWord(int idWord);
    public ArrayList<String> getAllExampleByIdMean(int idMean);
    public ArrayList<String> getAllCategoryByIdMean(int idMean);
    public ArrayList<String> getAllCategories();
    public ArrayList<Translation> getAllTranslations();
    public ArrayList<Word> getWordsByCategories(int idTranslation, ArrayList<String> listCategory);

}
