package com.example.alessandro.glossary.fragment.searchwords;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.alessandro.glossary.R;

/**
 * Created by Alessandro on 11/10/2016.
 */
public class TabsPageAdapterSearchWord extends FragmentPagerAdapter {
    private Context ctx;

    public TabsPageAdapterSearchWord(FragmentManager fm, Context ctx) {
        super(fm);
        this.ctx = ctx;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                Fragment fSearchWord = new SubFragmentSearchWordByName();
                return fSearchWord;
            case 1:
                Fragment fSearchWordByCategory = new SubFragmentSearchWordsByCategories();
                return fSearchWordByCategory;
            default:
                return null;
        }
    }


    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position){
        switch(position){
            case 0:
                return ctx.getString(R.string.srcword);
            case 1:
                return ctx.getString(R.string.srcategory);

            default:
                return null;
        }
    }
}
