package com.example.alessandro.glossary.fragment.insertword;

import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageButton;

import java.io.File;

/**
 * Created by Alessandro on 04/11/2016.
 */
public class DeleteListener implements View.OnClickListener {
    private Chronometer chr;
    private ImageButton btnPlayer;
    private ImageButton btnResetAudio;
    private String pathTmpSound;

    public DeleteListener(String pathTmpSound,Chronometer chr, ImageButton btnPlayer, ImageButton btnResetAudio) {
        this.chr = chr;
        this.btnPlayer = btnPlayer;
        this.btnResetAudio = btnResetAudio;
        this.pathTmpSound = pathTmpSound;
    }

    @Override
    public void onClick(View v) {
        File f0 = new File(pathTmpSound);
        boolean d0 = f0.delete();
        if(d0){
            chr.setVisibility(View.INVISIBLE);
            btnPlayer.setVisibility(View.INVISIBLE);
            btnResetAudio.setVisibility(View.INVISIBLE);
        }
        Log.w("Delete Check: ", ""+d0);
    }

}
