package com.example.alessandro.glossary.model;

import java.util.ArrayList;


public class Word {
    private int id;
    private Translation translation;
    private String name;
    private long timeMillisecondDate;
    private ArrayList<Mean> means;
    private String soundPathWord;


    public Word(){

    }

    public Word(Translation translation, String name, String soundPathWord, long timeMillisecond, ArrayList<Mean> means) {
        this.translation = translation;
        this.name = name;
        this.timeMillisecondDate = timeMillisecond;
        this.means = means;
        this.soundPathWord = soundPathWord;
    }

    public Translation getTranslation() {
        return translation;
    }

    public void setTranslation(Translation translation) {
        this.translation = translation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTimeMillisecondDate() {
        return timeMillisecondDate;
    }

    public void setTimeMillisecondDate(long timeMillisecondDate) {
        this.timeMillisecondDate = timeMillisecondDate;
    }

    public ArrayList<Mean> getMeans() {
        return means;
    }

    public void setMeans(ArrayList<Mean> means) {
        this.means = means;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSoundPathWord() {
        return soundPathWord;
    }

    public void setSoundPathWord(String soundPathWord) {
        this.soundPathWord = soundPathWord;
    }

}
