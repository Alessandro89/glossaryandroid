package com.example.alessandro.glossary.fragment.insertword.listener;

import com.example.alessandro.glossary.model.Mean;

import java.util.ArrayList;

/**
 * Created by Alessandro on 04/10/2016.
 */
public interface ListMeanListener {
    public void setListMeans(ArrayList<Mean> listMeans);
    public ArrayList<Mean> getListMeans();
}
