package com.example.alessandro.glossary.fragment.searchwords;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alessandro.database.DbGlossaryContract;
import com.example.alessandro.glossary.GlossaryActivity;
import com.example.alessandro.glossary.ModelDbWord;
import com.example.alessandro.glossary.R;
import com.example.alessandro.glossary.fragment.showwords.AdapterRecyclerShowMean;
import com.example.alessandro.glossary.model.Mean;
import com.example.alessandro.glossary.model.Translation;
import com.example.alessandro.glossary.model.Word;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.w3c.dom.Text;

import java.lang.reflect.Type;
import java.util.ArrayList;


public class SubFragmentSearchWordsByCategories extends Fragment implements AdapterRecyclerCategoryChecked.SearchCategoryListener {

    private Activity myActivity;
    private EditText wordSearched;
    private ModelDbWord mdw;
    private RecyclerView recyclerViewCategoryChecked;
    private ModelDbWord modelDbWord;
    private AdapterRecyclerCategoryChecked categoryCheckAdapter;
    private AdapterRecyclerWordsByCategory wordsCategoryAdapter;
    private TextView resultView;

    public SubFragmentSearchWordsByCategories() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        mdw = new ModelDbWord(myActivity.getApplicationContext());
        modelDbWord = new ModelDbWord(myActivity.getApplicationContext());

        ArrayList<String> listCategory = modelDbWord.getAllCategories();
        ArrayList<CategoryChecked> listCategoryChecked = getListCheckFalseCategory(listCategory);

        if(savedInstanceState!=null){ //recupero i dati che erano presenti prima della sua distruzione
            String gsonListCategoryChecked = savedInstanceState.getString("gsonListCategoryChecked");
            Type typelistCategoryChecked = new TypeToken<ArrayList<CategoryChecked>>() {}.getType();
            Type typelistWordByCategory = new TypeToken<ArrayList<Word>>() {}.getType();

            ArrayList<CategoryChecked> listCategoryCheckedTrue = new Gson().fromJson(gsonListCategoryChecked, typelistCategoryChecked);
            updateListCategoryChecked(listCategoryChecked, listCategoryCheckedTrue);
            categoryCheckAdapter =  new AdapterRecyclerCategoryChecked(listCategoryChecked, myActivity,this);

            String gsonListWordByCategory = savedInstanceState.getString("gsonListWordByCategory");
            ArrayList<Word> listWordByCategory = new Gson().fromJson(gsonListWordByCategory, typelistWordByCategory);
            wordsCategoryAdapter = new AdapterRecyclerWordsByCategory(listWordByCategory, myActivity);
        }


        //se non abbiamo dati e non c'è alcun stato salvato.. (primo avvio):
        if(categoryCheckAdapter==null && savedInstanceState==null){
            categoryCheckAdapter =  new AdapterRecyclerCategoryChecked(listCategoryChecked, myActivity,this);
        }
        else{
            //altrimenti c'era già il fragment ma abbiamo navigato con il backstack,
            // quindi wordsCategoryAdapter e categoryCheckAdapter contengono sicuramente qualcosa..
            //se per caso wordscategory è null lo inizializzo
            if(wordsCategoryAdapter==null) //controllo per sicurezza
                wordsCategoryAdapter = new AdapterRecyclerWordsByCategory(new ArrayList<Word>(), myActivity);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_words_bycategory, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //aggiorno le viste..
        //recycleViewCategory in ogni caso:
        recyclerViewCategoryChecked = (RecyclerView) myActivity.findViewById(R.id.recyclerViewCategoryCheck);
        recyclerViewCategoryChecked.setLayoutManager(new LinearLayoutManager(myActivity));
        recyclerViewCategoryChecked.setAdapter(categoryCheckAdapter);

        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(this.getContext(),
                LinearLayoutManager.VERTICAL);
        recyclerViewCategoryChecked.addItemDecoration(mDividerItemDecoration);

        //la lista delle parole se ci sono:
        if(wordsCategoryAdapter!=null){//non e' il primo avvio, e ci sono delle parole
            if(wordsCategoryAdapter.getItems().size()>0) {
                resultView = (TextView) myActivity.findViewById(R.id.results);
                resultView.setVisibility(View.VISIBLE);
            }

            RecyclerView recyclerViewWord = (RecyclerView) myActivity.findViewById(R.id.recyclerViewWordsByCategory);
            recyclerViewWord.setLayoutManager(new LinearLayoutManager(myActivity));
            recyclerViewWord.setAdapter(wordsCategoryAdapter);

            mDividerItemDecoration = new DividerItemDecoration(this.getContext(),
                    LinearLayoutManager.VERTICAL);
            recyclerViewWord.addItemDecoration(mDividerItemDecoration);

        }

        //Button btnSearch = (Button) myActivity.findViewById(R.id.btnSearchWordForCategory);
        //btnSearch.setOnClickListener(this);

    }

    //aggiorniamo la lista inplace
    private void updateListCategoryChecked(ArrayList<CategoryChecked> listCategoryChecked, ArrayList<CategoryChecked> listCategoryCheckedTrue) {

        for(CategoryChecked catCheck: listCategoryChecked){
            //se la categoria è presente nelle categorie checkate a true:
            if(findCategoryInList(catCheck.getCategory(),listCategoryCheckedTrue)!=null){
                catCheck.setChecked(true);
            }
        }
    }

    private CategoryChecked findCategoryInList(String categorySearch, ArrayList<CategoryChecked> listCategoryCheckedTrue){
        for(CategoryChecked categoryCheckedTrue: listCategoryCheckedTrue){
            if(categoryCheckedTrue.getCategory().equals(categorySearch))
                return categoryCheckedTrue;
        }
        //non ho trovato niente:
        return null;
    }

    private ArrayList<CategoryChecked> getListCheckFalseCategory(ArrayList<String> listCategory){
        ArrayList<CategoryChecked> listCategoryChecked = new ArrayList<>();
        for(String category: listCategory){
            listCategoryChecked.add(new CategoryChecked(category,false));
        }
        return listCategoryChecked;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);

        try {
            //mi aspetto che il context sia come istanza una activity
            this.myActivity = (Activity) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }


    private ArrayList<String> getNameCategories(ArrayList<CategoryChecked> listCategoryChecked){
        ArrayList<String> listCategory = new ArrayList<String>();
        for(CategoryChecked categoryCheck: listCategoryChecked){
            listCategory.add(categoryCheck.getCategory());
        }
        return listCategory;
    }

    private void showToast(String message) {
        Toast toast = Toast.makeText(myActivity, message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    @Override
    public void onSaveInstanceState(Bundle outState){

        Gson gson = new Gson();
        if (wordsCategoryAdapter == null) //se non è stato inizializzato:
            wordsCategoryAdapter = new AdapterRecyclerWordsByCategory(new ArrayList<Word>(), myActivity);

        String gsonWordsByCategory = gson.toJson(wordsCategoryAdapter.getItems());
        String gsonCategoryChecked = gson.toJson(categoryCheckAdapter.getItemsChecked());
        outState.putString("gsonListWordByCategory", gsonWordsByCategory);
        outState.putString("gsonListCategoryChecked", gsonCategoryChecked);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClickCategory() {
        recyclerViewCategoryChecked = (RecyclerView) myActivity.findViewById(R.id.recyclerViewCategoryCheck);

        Translation tr = ((GlossaryActivity) getActivity()).getCurrentTranslation();
        //se questo metodo viene chiamato dall'adapter.. forse si può semplificare la gestione.. da valutare
        ArrayList<CategoryChecked> listCategoryChecked = categoryCheckAdapter.getItemsChecked();
        ArrayList<Word> wordsByCategories = modelDbWord.getWordsByCategories(tr.getIdTranslation(), getNameCategories(listCategoryChecked));

        resultView = (TextView) myActivity.findViewById(R.id.results);
        if(wordsByCategories.size()>0) {
            resultView.setVisibility(View.VISIBLE);
        }
        else{
            resultView.setVisibility(View.INVISIBLE);
        }

        RecyclerView recyclerViewWord = (RecyclerView) myActivity.findViewById(R.id.recyclerViewWordsByCategory);
        wordsCategoryAdapter =  new AdapterRecyclerWordsByCategory(wordsByCategories, myActivity);
        recyclerViewWord.setLayoutManager(new LinearLayoutManager(myActivity));
        recyclerViewWord.setAdapter(wordsCategoryAdapter);

        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(this.getContext(),
                LinearLayoutManager.VERTICAL);
        recyclerViewWord.addItemDecoration(mDividerItemDecoration);
    }
}