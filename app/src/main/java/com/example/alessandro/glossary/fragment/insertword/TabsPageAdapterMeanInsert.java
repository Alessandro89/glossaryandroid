package com.example.alessandro.glossary.fragment.insertword;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.alessandro.glossary.R;

/**
 * Created by Alessandro on 27/09/2016.
 */
public class TabsPageAdapterMeanInsert extends FragmentPagerAdapter{

    private Context ctx;
    private int positionListMean;
    private String word;

    public TabsPageAdapterMeanInsert(FragmentManager fm, Context ctx, int positionListMean, String word) {
        super(fm);
        this.ctx = ctx;
        this.positionListMean = positionListMean;
        this.word = word;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle b = new Bundle();
        b.putInt("positionOfList", positionListMean);
        b.putString("wordToInsert",word);
        switch (position){
            case 0:
                Fragment fExample = new SubFragmentInsertExample();
                fExample.setArguments(b);
                return fExample;
            case 1:
                Fragment fCategory = new SubFragmentInsertCategory();
                fCategory.setArguments(b);
                return fCategory;
            default:
                return null;
        }
    }


    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position){
        switch(position){
            case 0:
                return ctx.getString(R.string.example);
            case 1:
                return ctx.getString(R.string.category);

            default:
                return null;
        }
    }
}

