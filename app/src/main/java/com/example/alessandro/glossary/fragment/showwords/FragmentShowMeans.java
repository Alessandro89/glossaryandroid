package com.example.alessandro.glossary.fragment.showwords;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.alessandro.glossary.ModelDbWord;
import com.example.alessandro.glossary.R;
import com.example.alessandro.glossary.model.Mean;

import java.util.ArrayList;


public class FragmentShowMeans extends Fragment implements View.OnClickListener{


    public FragmentShowMeans() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_showmeans_word, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FragmentActivity thisactivity = getActivity();
        ImageButton btnBack = (ImageButton) thisactivity.findViewById(R.id.backButton);
        TextView showWord = (TextView) thisactivity.findViewById(R.id.showWord);

        btnBack.setOnClickListener(this);

        int idWord = -1;
        if(getArguments()!=null){ //gli arguments rimangono anche dopo il destroyview.
            idWord = getArguments().getInt("idWord");
            showWord.append(getArguments().getString("nameWord"));
        }

        ModelDbWord modelDbWord = new ModelDbWord(thisactivity.getApplicationContext());

        ArrayList<Mean> listMeans = modelDbWord.lightGetAllMeansByIdWord(idWord);
        RecyclerView meansRecyclerView = (RecyclerView) thisactivity.findViewById(R.id.recyclerViewShowMeans);
        AdapterRecyclerShowMean meansWordAdapterRecycler = new AdapterRecyclerShowMean(listMeans, thisactivity);
        meansRecyclerView.setLayoutManager(new LinearLayoutManager(thisactivity));
        meansRecyclerView.setAdapter(meansWordAdapterRecycler);

        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(this.getContext(),
                LinearLayoutManager.VERTICAL);
        meansRecyclerView.addItemDecoration(mDividerItemDecoration);
    }

    @Override
    public void onStart(){
        super.onStart();
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.backButton){
            getActivity().onBackPressed();
        }
    }










}