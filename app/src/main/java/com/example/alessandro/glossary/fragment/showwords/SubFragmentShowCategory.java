package com.example.alessandro.glossary.fragment.showwords;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alessandro.glossary.ModelDbWord;
import com.example.alessandro.glossary.R;

import java.util.ArrayList;
import java.util.List;


public class SubFragmentShowCategory extends Fragment{

    private RecyclerView recyclerViewCategory;
    private AdapterRecyclerShowCategory categoryAdapterRecycler;
    private int idMean;
    private Activity myActivity;

    public SubFragmentShowCategory() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_showdatamean_category, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(getArguments()!=null){ //gli arguments rimangono anche dopo il destroyview.
            this.idMean = getArguments().getInt("idMean");
        }

        ModelDbWord mdw = new ModelDbWord(myActivity.getApplicationContext());

        List<String> categoryList = mdw.getAllCategoryByIdMean(idMean);
        categoryAdapterRecycler = new AdapterRecyclerShowCategory((ArrayList<String>) categoryList, myActivity);

        recyclerViewCategory = (RecyclerView) myActivity.findViewById(R.id.recyclerViewCategory);
        recyclerViewCategory.setLayoutManager(new LinearLayoutManager(myActivity));
        recyclerViewCategory.setVerticalScrollBarEnabled(true);
        recyclerViewCategory.setAdapter(categoryAdapterRecycler);

        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(this.getContext(),
                LinearLayoutManager.VERTICAL);
        recyclerViewCategory.addItemDecoration(mDividerItemDecoration);


    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);

        try {
            //mi aspetto che il context sia come istanza una activity
            this.myActivity = (Activity) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }




}