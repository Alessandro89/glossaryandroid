package com.example.alessandro.glossary.fragment.insertword.listener;

import android.content.Context;
import android.content.res.ColorStateList;
import android.drm.DrmManagerClient;
import android.graphics.Color;
import android.media.Image;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;

import com.example.alessandro.glossary.R;

import java.io.File;
import java.io.IOException;
import java.text.Format;

/**
 * Created by Alessandro on 04/11/2016.
 */
public class RecorderListener implements View.OnClickListener {
    private boolean startRecord = false;
    private MediaRecorder mediaRecorder = null;
    private Chronometer chronometer;
    private ImageButton playBtn;
    private ImageButton deleteBtn;
    private Button btnAction;
    private static final int DURATIONRECORDING = 15000;
    private String pathTmpSound;

    public RecorderListener(String pathTmpSound, Chronometer chronometer, ImageButton playBtn, ImageButton deleteBtn,  Button btnAction) {
        this.chronometer = chronometer;
        this.playBtn = playBtn;
        this.deleteBtn = deleteBtn;
        this.pathTmpSound = pathTmpSound;
        this.btnAction = btnAction;
    }


    @Override
    public void onClick(View v) {
        if(!startRecord) //se non sta registrando inizia
            startRecord((ImageButton) v);
        else //se sta registrando stoppa:
            stopRecord((ImageButton) v);
    }

    private void startRecord(final ImageButton btnRecorder) {
        mediaRecorder = new MediaRecorder();

        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setOutputFile(pathTmpSound);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mediaRecorder.setMaxDuration(DURATIONRECORDING); //15 secondi massimi

        try {
            mediaRecorder.prepare();
        } catch (IOException e) {
            Log.d("recorder: ", "prepare() failed");
        }

        mediaRecorder.start();

        //non voglio che fai delle determinati azioni (come l'inserimento di una parola mentre stai registrando..)
        if(btnAction!=null)
            btnAction.setEnabled(false);

        if(chronometer.getVisibility()==View.INVISIBLE) //check utile?
            chronometer.setVisibility(View.VISIBLE);

        chronometer.start();
        chronometer.setBase(SystemClock.elapsedRealtime());
        btnRecorder.setColorFilter(Color.parseColor("#A60626"));
        startRecord = true;
        Log.d("recorder: ", "start!");

        //quando raggiunge il limite, si deve stoppare..
        mediaRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
            @Override
            public void onInfo(MediaRecorder mr, int what, int extra) {
                //se hai raggiunto il massimo, stoppiamo
                if(what==MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
                    stopRecord(btnRecorder);
                }
            }
        });
    }

    private void stopRecord(ImageButton btnRecorder) {

        mediaRecorder.release();
        chronometer.stop();
        if(btnAction!=null)
            btnAction.setEnabled(true);
        btnRecorder.clearColorFilter();
        mediaRecorder = null;
        startRecord = false;
        if(new File(pathTmpSound).exists()) { //controllo abbastanza inutile (c'è di sicuro)..
            playBtn.setVisibility(View.VISIBLE);
            deleteBtn.setVisibility(View.VISIBLE);
        }
        Log.d("recorder: ", "stop!");
    }
}
