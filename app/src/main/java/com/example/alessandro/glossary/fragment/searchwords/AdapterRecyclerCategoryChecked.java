package com.example.alessandro.glossary.fragment.searchwords;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.alessandro.glossary.R;
import com.example.alessandro.glossary.fragment.AdapterRecycler;

import java.util.ArrayList;

/**
 * Created by Alessandro on 21/09/2016.
 */
public class AdapterRecyclerCategoryChecked extends RecyclerView.Adapter<AdapterRecyclerCategoryChecked.ViewHolder>{

    private SearchCategoryListener searchCategoryListener;

    public interface SearchCategoryListener {
        public void onClickCategory();
    }
    private ArrayList<CategoryChecked> listCategoryChecked;
    private Activity activity;

    public ArrayList<CategoryChecked> getItems() {
        return listCategoryChecked;
    }
    public ArrayList<CategoryChecked> getItemsChecked() {
        ArrayList<CategoryChecked> listCategoryTrueCheck = new ArrayList<>();
        for(CategoryChecked categoryChecked: listCategoryChecked){
            if(categoryChecked.isChecked()){ //se è true:
                listCategoryTrueCheck.add(categoryChecked);
            }
        }
        return listCategoryTrueCheck;
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView categoryTextView;
        public CheckBox checkboxCategory;
        public ViewHolder(View v) {
            super(v);
            categoryTextView = (TextView) v.findViewById(R.id.textViewCategory);
            checkboxCategory = (CheckBox) v.findViewById(R.id.checkBoxCategory);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterRecyclerCategoryChecked(ArrayList<CategoryChecked> listCategoryChecked, Activity activity, Fragment fragmentSearchCategory) {
        this.listCategoryChecked = listCategoryChecked;
        this.activity = activity;
        this.searchCategoryListener = (SearchCategoryListener) fragmentSearchCategory;
    }


    // Create new views (invoked by the layout manager)
    @Override
    public AdapterRecyclerCategoryChecked.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.itemlayout_recycleview_checkedcategory, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.categoryTextView.setText(listCategoryChecked.get(position).getCategory());

        holder.checkboxCategory.setChecked(listCategoryChecked.get(position).isChecked());
        holder.checkboxCategory.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                listCategoryChecked.get(position).setChecked(isChecked);
                searchCategoryListener.onClickCategory();
            }
        });
        //anche quando premi la riga voglio selezionare
        holder.itemView.setOnClickListener(new SelectRowListener(holder.checkboxCategory,position));

    }

    private class SelectRowListener implements  View.OnClickListener{
        private CheckBox checkBox;
        private int position;
        public SelectRowListener(CheckBox checkBox,int position){
            this.checkBox = checkBox;
            this.position = position;
        }
        @Override
        public void onClick(View v) {
            checkBox.setChecked(!listCategoryChecked.get(position).isChecked()); //e questo chiama l'altro listener della checkbox
        }
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return listCategoryChecked.size();
    }


}
