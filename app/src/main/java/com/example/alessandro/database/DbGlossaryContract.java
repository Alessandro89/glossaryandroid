package com.example.alessandro.database;

import android.provider.BaseColumns;

/**
 * Created by Alessandro on 19/07/2016.
 */
public final class DbGlossaryContract {
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor. (??)
    public DbGlossaryContract() {}

        /* Inner class that defines the table contents */
    //non abbiamo bisogno di BaseColumns, perchè non vogliamo un id, la chiave è Cognome
    //By implementing the BaseColumns interface, your inner class can inherit a primary key field called _ID

    //it-eng, eng-it, it-spain, ecc..
    public static abstract class Translation implements BaseColumns {
        public static final String TABLE_NAME = "translation";
        public static final String COLUMN_DESC = "description";
    }

    public static abstract class Word implements BaseColumns {
        public static final String TABLE_NAME = "word";
        public static final String COLUMN_NAMEWORD = "name";
        public static final String COLUMN_IDTRANSL = "idTranslation";
        public static final String COLUMN_DATA = "data";
        public static final String COLUMN_PATHSOUND = "pathSound";
    }

    public static abstract class WordMeans implements BaseColumns {
        public static final String TABLE_NAME = "wordMeans";
        public static final String COLUMN_IDWORD = "idWord";
        public static final String COLUMN_MEAN = "mean";
    }

    public static abstract class CategoryMeans implements BaseColumns {
        public static final String TABLE_NAME = "categoryMeans";
        public static final String COLUMN_IDMEAN = "idMean";
        public static final String COLUMN_IDCATEGORY = "idCategory";
    }

    public static abstract class ExampleMean implements BaseColumns {
        public static final String TABLE_NAME = "ExampleMean";
        public static final String COLUMN_IDMEAN = "idMean";
        public static final String COLUMN_EXAMPLE = "example";
    }

    public static abstract class Category implements BaseColumns {
        public static final String TABLE_NAME = "Category";
        public static final String COLUMN_DESCCATEGORY = "description";
    }



    public static final String SQL_CREATE_TRANSLATIONTABLE =
            "CREATE TABLE " +Translation.TABLE_NAME + " (" +
                    Translation._ID + " INTEGER PRIMARY KEY," +
                    Translation.COLUMN_DESC + " TEXT UNIQUE " +        //text, sei sicuro? non bisogna definire una dimensione?!
                    " )";


    public static final String SQL_CREATE_WORDTABLE =
            "CREATE TABLE " +Word.TABLE_NAME + " (" +
                    Word._ID + " INTEGER PRIMARY KEY," +
                    Word.COLUMN_NAMEWORD + " TEXT UNIQUE, "+
                    Word.COLUMN_IDTRANSL + " INTEGER, " +
                    Word.COLUMN_DATA + " NUMERIC, " +
                    Word.COLUMN_PATHSOUND + " TEXT " +
                    " )";


    public static final String SQL_CREATE_WORDMEANSTABLE =
            "CREATE TABLE " +WordMeans.TABLE_NAME + " (" +
                    ExampleMean._ID + " INTEGER PRIMARY KEY," +
                    WordMeans.COLUMN_IDWORD + " INTEGER," +
                    WordMeans.COLUMN_MEAN + " TEXT " +
                    " )";

    public static final String SQL_CREATE_CATEGORYMEANSABLE =
            "CREATE TABLE " + CategoryMeans.TABLE_NAME + " (" +
                    CategoryMeans.COLUMN_IDMEAN + " INTEGER," +
                    CategoryMeans.COLUMN_IDCATEGORY + " INTEGER, "+
                    "PRIMARY KEY (" + CategoryMeans.COLUMN_IDCATEGORY+ "," + CategoryMeans.COLUMN_IDMEAN + ")" +
                    " )";

    public static final String SQL_CREATE_EXAMPLEMEANSTABLE =
            "CREATE TABLE " + ExampleMean.TABLE_NAME + " (" +
                    ExampleMean._ID + " INTEGER PRIMARY KEY," +
                    ExampleMean.COLUMN_IDMEAN + " INTEGER, " +
                    ExampleMean.COLUMN_EXAMPLE + " TEXT "+
                    " )";

    public static final String SQL_CREATE_CATEGORY =
            "CREATE TABLE " + Category.TABLE_NAME + " (" +
                    Category._ID + " INTEGER PRIMARY KEY," +
                    Category.COLUMN_DESCCATEGORY + " TEXT UNIQUE"+
                    " )";



}




