package com.example.alessandro.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.alessandro.exception.MyExceptionInsert;
import com.example.alessandro.glossary.IModelWord;
import com.example.alessandro.glossary.model.Mean;
import com.example.alessandro.glossary.model.Translation;
import com.example.alessandro.glossary.model.Word;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alessandro on 19/07/2016.
 */
public class HelperDbGlossary extends SQLiteOpenHelper implements IModelWord {
    // If you change the database schema, you must increment the database version!
    public static final int DATABASE_VERSION = 1; //prima versione schema
    public static final String DATABASE_NAME = "GlossaryWord.db";
    public Context context;

    private static final String SQL_DROPDB=
    "DROP DATABASE " + "GlossaryWord";

    public HelperDbGlossary(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    //Called when the database is created for the first time.
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DbGlossaryContract.SQL_CREATE_TRANSLATIONTABLE);
        db.execSQL(DbGlossaryContract.SQL_CREATE_WORDTABLE);
        db.execSQL(DbGlossaryContract.SQL_CREATE_WORDMEANSTABLE);
        db.execSQL(DbGlossaryContract.SQL_CREATE_CATEGORYMEANSABLE);
        db.execSQL(DbGlossaryContract.SQL_CREATE_EXAMPLEMEANSTABLE);
        db.execSQL(DbGlossaryContract.SQL_CREATE_CATEGORY);

        insertTranslationsForSetUp(db);
    }

    public void deleteDatabaseForTest(){
        context.deleteDatabase(DATABASE_NAME);
    }

    private void insertTranslationsForSetUp(SQLiteDatabase db){
        //il db l'ha già aperto il db (this.getWritableDatabase non è necessario)
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();

        values.put(DbGlossaryContract.Translation.COLUMN_DESC, "eng-it");
        db.insertOrThrow(DbGlossaryContract.Translation.TABLE_NAME, null, values);

        values.clear(); //Removes all values
        values.put(DbGlossaryContract.Translation.COLUMN_DESC, "it-eng");
        db.insertOrThrow(DbGlossaryContract.Translation.TABLE_NAME, null, values);

    }

    @Override
    public void insertWord(Word wd) throws MyExceptionInsert {
        //transazione:
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try {
            db.beginTransaction();
            long idWord = InsertInWordTable(wd, db, values);
            InsertInMeanExampleCategory(wd, db, values, idWord);

            db.setTransactionSuccessful();
        } catch(Exception e) {
            e.printStackTrace(); //da restituire messaggio di errore
            throw new MyExceptionInsert("La parola non è stata inserita, controlla se esiste già");
        } finally {
            //finally sarà eseguito anche se lancio l'eccezione
            db.endTransaction();
        }

    }

    private long insertInCategory(SQLiteDatabase db, ContentValues values, String category){
        values.clear();
        values.put(DbGlossaryContract.Category.COLUMN_DESCCATEGORY, category);
        return db.insertOrThrow(DbGlossaryContract.Category.TABLE_NAME, null, values);
    }

    private void InsertInMeanExampleCategory(Word wd, SQLiteDatabase db, ContentValues values, long idWord) {
        for (Mean mean : wd.getMeans()){
            values.clear();
            values.put(DbGlossaryContract.WordMeans.COLUMN_IDWORD, idWord);
            values.put(DbGlossaryContract.WordMeans.COLUMN_MEAN, mean.getMean());
            long idmean = db.insertOrThrow(DbGlossaryContract.WordMeans.TABLE_NAME, null, values);
            //per ogni mean, dobbiamo inserire i suoi esempi e le sue categorie!
            insertExamples(mean, db, values, idmean);
            insertCategoryMean(mean, db, values, idmean);

        }
    }
    private void insertCategoryMean(Mean mean, SQLiteDatabase db, ContentValues values, long idmean){
        HashMap<Long, String>  listMapCategories = getListMapCategories();
        Long idCategory;
        for (String category: mean.getCategories()){
            idCategory = getIdCategories(listMapCategories, category);
            if(idCategory == null) { //Se è una categoria nuova la inserisco
                idCategory = insertInCategory(db, values, category);
            }

            //e per ogni glossario associo la parola che stiamo inserendo..
            values.clear();
            values.put(DbGlossaryContract.CategoryMeans.COLUMN_IDCATEGORY, idCategory);
            values.put(DbGlossaryContract.CategoryMeans.COLUMN_IDMEAN, idmean);
            db.insertOrThrow(DbGlossaryContract.CategoryMeans.TABLE_NAME, null, values);
        }
    }

    private void insertExamples(Mean mean, SQLiteDatabase db, ContentValues values, long idmean) {
         for(String example : mean.getExamples()){
             values.clear();
             values.put(DbGlossaryContract.ExampleMean.COLUMN_IDMEAN, idmean);
             values.put(DbGlossaryContract.ExampleMean.COLUMN_EXAMPLE, example);
             db.insertOrThrow(DbGlossaryContract.ExampleMean.TABLE_NAME, null, values);
         }
    }



    private Long getIdCategories(HashMap<Long, String> listMapCategories, String category){
        for(Map.Entry<Long,String> elCategory: listMapCategories.entrySet()){
            if(elCategory.getValue().equals(category)){
                return elCategory.getKey();
            }
        }
        //non ho trovato niente, restituisco null
        return null;
    }

    private HashMap<Long, String>  getListMapCategories(){
        HashMap<Long, String> listMapCategories = new HashMap<Long, String>();
        SQLiteDatabase db = this.getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                DbGlossaryContract.Category._ID,
                DbGlossaryContract.Category.COLUMN_DESCCATEGORY
        };


        Cursor c = db.query(
                DbGlossaryContract.Category.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                null,                                     // The columns for the WHERE clause
                null,                                      // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                      // don't sort the order
        );


        //Move the cursor to the first row.
        //This method will return false if the cursor is empty.
        if(c.moveToFirst()==false)
            return new HashMap<Long, String>(); //lista vuota (nessun glossario presente)

        do {
            long idCategory = c.getLong(c.getColumnIndex(DbGlossaryContract.Category._ID));
            String category = c.getString(c.getColumnIndex(DbGlossaryContract.Category.COLUMN_DESCCATEGORY));
            listMapCategories.put(idCategory, category);
        }while(c.moveToNext());//This method will return false if the cursor is already past the last entry in the result set.
        c.close();

        return listMapCategories;
    }

    private long InsertInWordTable(Word wd, SQLiteDatabase db, ContentValues values) throws ParseException {
        values.put(DbGlossaryContract.Word.COLUMN_NAMEWORD, wd.getName());
        values.put(DbGlossaryContract.Word.COLUMN_DATA, wd.getTimeMillisecondDate());
        values.put(DbGlossaryContract.Word.COLUMN_IDTRANSL, wd.getTranslation().getIdTranslation());
        values.put(DbGlossaryContract.Word.COLUMN_PATHSOUND, wd.getSoundPathWord());
        return db.insertOrThrow(DbGlossaryContract.Word.TABLE_NAME, null,values);
    }


    //restituisco la parola con la sua lista di significati, di glossari, di contesti
    public Word lightGetWordByName(String wordStr) {
        Word word = new Word();
        SQLiteDatabase db = this.getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.

        String[] projection = {
                DbGlossaryContract.Word._ID,
                DbGlossaryContract.Word.COLUMN_NAMEWORD,
                DbGlossaryContract.Word.COLUMN_PATHSOUND
        };

        //String[] valuesWhere = { stringa... };

        //http://stackoverflow.com/questions/20777533/sqlite-cannot-bind-argument-at-index-1-because-the-index-is-out-of-range-the-s
        Cursor c = db.query(
                DbGlossaryContract.Word.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                DbGlossaryContract.Word.COLUMN_NAMEWORD + "=?",  // The columns for the WHERE clause
                new String[]{wordStr}, //valuesWhere,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // don't sort the order
        );

        if(c.moveToFirst()==false)
            return null;

        do {
            word.setId(c.getInt(c.getColumnIndex(DbGlossaryContract.Word._ID)));
            word.setName(c.getString(c.getColumnIndex(DbGlossaryContract.Word.COLUMN_NAMEWORD)));
            word.setSoundPathWord(c.getString(c.getColumnIndex(DbGlossaryContract.Word.COLUMN_PATHSOUND)));
        }while(c.moveToNext());
        c.close();
        return word;
    }


    @Override
    public ArrayList<String> getAllCategories() {
        ArrayList<String> listCategories = new ArrayList<String>();


        SQLiteDatabase db = this.getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.

        String[] projection = {
                DbGlossaryContract.Category.COLUMN_DESCCATEGORY,
        };


        Cursor c = db.query(
                DbGlossaryContract.Category.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                null,                     // The columns for the WHERE clause
                null, //valuesWhere,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // don't sort the order
        );

        //Move the cursor to the first row.
        //This method will return false if the cursor is empty.
        if(c.moveToFirst()==false)
            return listCategories; //empty array


        do {
            listCategories.add(c.getString(c.getColumnIndex(DbGlossaryContract.Category.COLUMN_DESCCATEGORY)));
        }while(c.moveToNext());//This method will return false if the cursor is already past the last entry in the result set.
        c.close();
        return listCategories;
    }

    @Override
    public ArrayList<Translation> getAllTranslations() {
        ArrayList<Translation> listTranslation = new ArrayList<>();


        SQLiteDatabase db = this.getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                DbGlossaryContract.Translation._ID,
                DbGlossaryContract.Translation.COLUMN_DESC
        };

         Cursor c = db.query(
                DbGlossaryContract.Translation.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                null,                                   // The columns for the WHERE clause
                null,
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // don't sort the order
        );

        //Move the cursor to the first row.
        //This method will return false if the cursor is empty.
        if(c.moveToFirst()==false)
            return listTranslation; //empty array


        do {
            Translation t = new Translation();
            t.setIdTranslation(Integer.parseInt(c.getString(c.getColumnIndex(DbGlossaryContract.Translation._ID))));
            t.setDescription(c.getString(c.getColumnIndex(DbGlossaryContract.Translation.COLUMN_DESC)));
            listTranslation.add(t);
        }while(c.moveToNext());//This method will return false if the cursor is already past the last entry in the result set.
        c.close();
        return listTranslation;
    }

    //do solo quello che serve, performance migliori
    @Override
    public ArrayList<Word> lightGetAllDailyWords(int idTranslation) {

        ArrayList<Word> listWords = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String[] projection = {
                DbGlossaryContract.Word._ID,
                DbGlossaryContract.Word.COLUMN_NAMEWORD,
                DbGlossaryContract.Word.COLUMN_DATA,
                DbGlossaryContract.Word.COLUMN_PATHSOUND
        };

        Cursor c = db.query(
                DbGlossaryContract.Word.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                DbGlossaryContract.Word.COLUMN_IDTRANSL + "=?", //where
                new String[]{""+idTranslation}, //valuesWhere,
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                DbGlossaryContract.Word.COLUMN_DATA + " DESC" //orderby
        );

        if(c.moveToFirst()==false)
            return listWords; //empty array

        do {
            String wordStr = c.getString(c.getColumnIndex(DbGlossaryContract.Word.COLUMN_NAMEWORD));
            String pathSoundWord = c.getString(c.getColumnIndex(DbGlossaryContract.Word.COLUMN_PATHSOUND));
            long millisecondTime = c.getLong(c.getColumnIndex(DbGlossaryContract.Word.COLUMN_DATA));
            int id = c.getInt(c.getColumnIndex(DbGlossaryContract.Word._ID));
            Word word = new Word();
            word.setId(id);
            word.setTimeMillisecondDate(millisecondTime);
            word.setName(wordStr);
            word.setSoundPathWord(pathSoundWord);
            listWords.add(word);
        }while(c.moveToNext());//This method will return false if the cursor is already past the last entry in the result set.
        c.close();
        return listWords;

    }

    @Override
    public ArrayList<Mean> lightGetAllMeansByIdWord(int idWord) {
        ArrayList<Mean> listMean = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String[] projection = {
                DbGlossaryContract.WordMeans._ID,
                DbGlossaryContract.WordMeans.COLUMN_MEAN
        };

        Cursor c = db.query(
                DbGlossaryContract.WordMeans.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                DbGlossaryContract.WordMeans.COLUMN_IDWORD + "=?", //where
                new String[]{""+idWord},
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null
        );

        if(c.moveToFirst()==false)
            return listMean; //empty array

        do {
            String meanStr = c.getString(c.getColumnIndex(DbGlossaryContract.WordMeans.COLUMN_MEAN));
            int idMean = c.getInt(c.getColumnIndex(DbGlossaryContract.WordMeans._ID));
            Mean mean = new Mean();
            mean.setId(idMean);
            mean.setMean(meanStr);
            listMean.add(mean);
        }while(c.moveToNext());//This method will return false if the cursor is already past the last entry in the result set.
        c.close();
        return listMean;
    }

    @Override
    public ArrayList<String> getAllExampleByIdMean(int idMean) {
        ArrayList<String> listExample = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String[] projection = {
                DbGlossaryContract.ExampleMean.COLUMN_EXAMPLE
        };

        Cursor c = db.query(
                DbGlossaryContract.ExampleMean.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                DbGlossaryContract.ExampleMean.COLUMN_IDMEAN + "=?", //where
                new String[]{"" + idMean},
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null
        );

        if(c.moveToFirst()==false)
            return listExample; //empty array

        do {
            String exampleStr = c.getString(c.getColumnIndex(DbGlossaryContract.ExampleMean.COLUMN_EXAMPLE));
            listExample.add(exampleStr);
        }while(c.moveToNext());//This method will return false if the cursor is already past the last entry in the result set.
        c.close();
        return listExample;
    }

    @Override
     public ArrayList<String> getAllCategoryByIdMean(int idMean) {
        ArrayList<String> listCategory = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        //join category e categoryMeans e prendo solo quelli riferenti a un certo idMean
        String sqlcategoriesByMean = "SELECT category.description FROM " + DbGlossaryContract.CategoryMeans.TABLE_NAME + " categoryMeans "+
                "INNER JOIN "+ DbGlossaryContract.Category.TABLE_NAME + " category ON "
                + "category._id = categoryMeans.idCategory "+
                "WHERE categoryMeans.idMean= ?";

        //da testare..
        Cursor c =   db.rawQuery(sqlcategoriesByMean, new String[]{"" + idMean});

        if(c.moveToFirst()==false)
            return listCategory; //empty array

        do {
            String categoryStr = c.getString(c.getColumnIndex(DbGlossaryContract.Category.COLUMN_DESCCATEGORY));
            listCategory.add(categoryStr);
        }while(c.moveToNext());//This method will return false if the cursor is already past the last entry in the result set.
        c.close();
        return listCategory;
    }


    @Override
    public ArrayList<Word> getWordsByCategories(int idTranslation, ArrayList<String> listCategory) {
        //todo da verificare:
        ArrayList<Word> listWord = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        //ti estrarrà più volte la stessa parola.. ma servono i vari mean di ogni parola
        /*String sqlWordByCategory = "SELECT word._id, word.name, wordmeans._id, wordmeans.mean FROM " +
                DbGlossaryContract.Word.TABLE_NAME + " INNER JOIN " + DbGlossaryContract.WordMeans.TABLE_NAME + " wordmeans" +
                " ON word._id = wordmeans.idWord "+ " INNER JOIN " + DbGlossaryContract.CategoryMeans.TABLE_NAME + " categorymeans " +
                " ON wordmeans._id = categorymeans.idMean INNER JOIN "+ DbGlossaryContract.Category.TABLE_NAME + " category" +
                " ON categorymeans.idcategory = category._id AND category.description IN (" + formatListCategory(listCategory) + ")" +
                " WHERE word.idTranslation = ?" +
                " GROUP BY word.id, word.name, wordmeans._id, wordmeans.mean";*/
        String sqlWordByCategory = "SELECT word._id as idWord, word.name, word.pathSound, wordmeans._id as idMean, wordmeans.mean FROM " +
                DbGlossaryContract.Word.TABLE_NAME + " INNER JOIN " + DbGlossaryContract.WordMeans.TABLE_NAME + " wordmeans" +
                " ON word._id = wordmeans.idWord "+ " INNER JOIN " + DbGlossaryContract.CategoryMeans.TABLE_NAME + " categorymeans " +
                " ON wordmeans._id = categorymeans.idMean  INNER JOIN "+ DbGlossaryContract.Category.TABLE_NAME + " category" +
                " ON categorymeans.idcategory = category._id AND category.description IN (" + formatListCategory(listCategory) + ")" +
                " WHERE word.idTranslation = ?" +
                " GROUP BY idWord, word.name, idMean, wordmeans.mean";
        //da testare..
        Cursor c =   db.rawQuery(sqlWordByCategory, new String[]{""+idTranslation});

        if(c.moveToFirst()==false)
            return listWord;

        do {
            Word wd = new Word();
            int wordid = c.getInt(c.getColumnIndex("idWord"));
            String wordname = c.getString(c.getColumnIndex(DbGlossaryContract.Word.COLUMN_NAMEWORD));
            String pathSound = c.getString(c.getColumnIndex(DbGlossaryContract.Word.COLUMN_PATHSOUND));
            wd.setId(wordid);
            wd.setName(wordname);
            wd.setMeans(new ArrayList<Mean>());
            wd.setSoundPathWord(pathSound);

            //stessa parola aggiungiamo i mean:
            if(!listWord.isEmpty() && (listWord.get(listWord.size()-1).getId()==wordid)){
                //add mean to current word
                String meanStr = c.getString(c.getColumnIndex(DbGlossaryContract.WordMeans.COLUMN_MEAN));
                int meanId = c.getInt(c.getColumnIndex("idMean"));
                Mean mean = new Mean();
                mean.setMean(meanStr);
                mean.setId(meanId);
                //aggiungiamo i mean alla lista dei means
                listWord.get(listWord.size()-1).getMeans().add(mean);
            }else{ //nuova parola
                //mettiamo il primo significato della nuova parola (almeno 1 deve averlo:
                //altrimenti non sarebbe uscito nella inner join (per avere delle categorie devi avere dei significati nelle parola)
                int meanId = c.getInt(c.getColumnIndex("idMean"));
                String meanStr = c.getString(c.getColumnIndex(DbGlossaryContract.WordMeans.COLUMN_MEAN));
                Mean mean = new Mean();
                mean.setMean(meanStr);
                mean.setId(meanId);
                wd.getMeans().add(mean);
                listWord.add(wd);
            }

        }while(c.moveToNext());//This method will return false if the cursor is already past the last entry in the result set.
        c.close();
        return listWord;
    }

    private String formatListCategory(ArrayList<String> listCategory){
        String formatlistCategory = "";
        for(String category: listCategory){
            formatlistCategory +="'"+category + "',";
        }
        if(!formatlistCategory.isEmpty())
            formatlistCategory = formatlistCategory.substring(0, formatlistCategory.length()-1); //start e inclusive, end è esclusive

        return formatlistCategory;
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //??
    }
}

